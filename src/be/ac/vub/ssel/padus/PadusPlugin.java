/******************************************************************************
 * Copyright (c) 2006 Vrije Universiteit Brussel
 * All rights reserved.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0, which accompanies this
 * distribution, and is available at http://www.eclipse.org/legal/epl-v10.html.
 * 
 * Contributors:
 *    Niels Joncheere
 *    Mathieu Braem
 ******************************************************************************/
package be.ac.vub.ssel.padus;

import java.io.File;
import java.io.IOException;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.ui.plugin.AbstractUIPlugin;

/**
 * @author Niels Joncheere
 * @author Mathieu Braem
 */
public class PadusPlugin extends AbstractUIPlugin {
	
	public static final String FILE_SEPARATOR = System.getProperty("file.separator");
	
	/** The single plug-in instance */
	private static PadusPlugin instance;
	
	/** Returns the single plug-in instance. */
	public static PadusPlugin getInstance() {
		
		if (instance == null) {
			instance = new PadusPlugin();
		}
		return instance;
	}
	
	/**
	 * Retrieves the installation path of the plug-in.
	 * 
	 * @return The installation path
	 */
	public static String getPluginPath() {
		
		String pathName = "";
		try {
			pathName = FileLocator.resolve(getInstance().getBundle().getEntry("/")).getPath();
			File pathFile = new File(pathName);
			pathName = pathFile.getCanonicalPath();
		}
		catch (IOException ioe) {
			System.err.println(ioe.toString());
		}
		return pathName + FILE_SEPARATOR;
	}
	
	/**
	 * The constructor
	 * 
	 * This class does not completely implement the singleton design pattern,
	 * because the visibility of its constructor is not private.  Changing this
	 * would make the class incompatible with the Eclipse framework.
	 */
	public PadusPlugin() {
		
		instance = this;
	}
}
