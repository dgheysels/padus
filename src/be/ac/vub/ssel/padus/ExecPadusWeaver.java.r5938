/******************************************************************************
 * Copyright (c) 2006 Vrije Universiteit Brussel
 * All rights reserved.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0, which accompanies this
 * distribution, and is available at http://www.eclipse.org/legal/epl-v10.html.
 * 
 * Contributors:
 *    Niels Joncheere
 *    Mathieu Braem
 ******************************************************************************/
package be.ac.vub.ssel.padus;

import be.ac.vub.ssel.padus.util.NullOutputStream;
import be.ac.vub.ssel.padus.util.StreamGobbler;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * @author Niels Joncheere
 * @author Mathieu Braem
 */
public class ExecPadusWeaver extends PadusWeaver {
	
	public static void main(String[] args) {
		
		ExecPadusWeaver weaver = new ExecPadusWeaver();
		weaver.addAspect("FixedFeeBilling", "/java/workspaces/workspace/padus/demo/Fixed_Billing.xml");
		weaver.addAspect("GenericBilling", "/java/workspaces/workspace/padus/demo/Generic_Billing.xml");
		weaver.addAspect("Logging", "/java/workspaces/workspace/padus/demo/Logging.xml");
		weaver.addProcess("ConferenceCall", "/java/workspaces/workspace/padus/demo/ConferenceCall.bpel");
		weaver.setDeploymentFile("/java/workspaces/workspace/padus/demo/deployment.xml");
		weaver.setOutputFile("/java/workspaces/workspace/padus/demo/ConferenceCall-new.bpel");
		weaver.setWeaverFile("/java/workspaces/workspace/padus/pl/deployment.pl");
		weaver.setPrologInterpreter("/opt/local/lib/swipl-5.6.16/bin/i386-darwin8.7.1/swipl");
		weaver.weave();
	}
	
	private String prologInterpreter;
	
	private String addSlashes(String input) {
		
		String result = new String();
		for (int i = 0; i < input.length(); i++) {
			char current = input.charAt(i);
			result += current;
			if (current == '\\') {
				result += current;
			}
		}
		return result;
	}
	
	@Override
	protected void debug() {
		
		super.debug();
		
		System.out.println("  Prolog interpreter:");
		System.out.println("  - " + getPrologInterpreter());
		System.out.println();
	}
	
	/**
	 * Prints a definition instruction on the given print stream.
	 * 
	 * @param type the type of definition, either "Aspects" or "Processes"
	 * @param hash the key-value map, containing the aspects or processes
	 */
	private <K, V> String getDefinition(String type, Map<K, V> map) {
		
		List<K> keyList = new ArrayList<K>();
		List<V> valueList = new ArrayList<V>();
		for (Entry<K, V> entry : map.entrySet()) {
			keyList.add(entry.getKey());
			valueList.add(entry.getValue());
		}
		
		return "define" + type + "(" + getPrologList(keyList) + ", "
				+ getPrologList(valueList) + ").";
	}
	
	/**
	 * Prints a list in Prolog formatting on the given print stream.
	 */
	private <T> String getPrologList(List<T> list) {
		
		int size = list.size();
		
		if (size == 0) {
			return "[]";
		}
		
		String result = "['";
		T element = list.get(0);
		result += addSlashes(element.toString()) + "'";
		for (int i = 1; i < size; ++i) {
			result += ", '";
			element = list.get(i);
			result += addSlashes(element.toString()) + "'";
		}
		result += "]";
		
		return result;
	}
	
	protected String getPrologInterpreter() {
		
		return prologInterpreter;
	}
	
	public void setPrologInterpreter(String prologPath) {
		
		this.prologInterpreter = prologPath;
	}
	
	/**
	 * Invokes the weaver.  All added files need to be specified using an
	 * absolute path. 
	 */
	@Override
	protected void weaveInternal() {
		
		try {
			System.out.print("Weaving...");
			Runtime rt = Runtime.getRuntime();
			String[] command = new String[] { getPrologInterpreter() };
			Process weavingProcess = rt.exec(command);
			
			OutputStream os = weavingProcess.getOutputStream();
			PrintStream ps = new PrintStream(os);
			ps.println("consult('" + addSlashes(getWeaverFile()) + "').");
			ps.println(getDefinition("Aspects", getAspects()));
			ps.println(getDefinition("Processes", getProcesses()));
			ps.println("deploy('" + addSlashes(getDeploymentFile()) + "').");
			ps.println("halt.");
			ps.close();
			os.close();
			
			StreamGobbler processGobbler = new StreamGobbler(weavingProcess.getInputStream(), new PrintStream(getOutputFile()));
			StreamGobbler errGobbler = new StreamGobbler(weavingProcess.getErrorStream(), new PrintStream(new NullOutputStream()));
			processGobbler.start();
			errGobbler.start();
			
			weavingProcess.waitFor();
			System.out.println("OK");
		} catch (Exception e) {
			System.out.println("FAILED");
			// do nothing
		}
	}
}
