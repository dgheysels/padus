/******************************************************************************
 * Copyright (c) 2006 Vrije Universiteit Brussel
 * All rights reserved.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0, which accompanies this
 * distribution, and is available at http://www.eclipse.org/legal/epl-v10.html.
 * 
 * Contributors:
 *    Niels Joncheere
 *    Mathieu Braem
 ******************************************************************************/
package be.ac.vub.ssel.padus;

//import java.io.BufferedOutputStream;
//import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import jpl.Atom;
import jpl.Query;
import jpl.Term;
import jpl.Util;

/**
 * @author Niels Joncheere
 * @author Mathieu Braem
 */
public class JplPadusWeaver extends PadusWeaver {
	
	public static void main(String[] args) {
		
		JplPadusWeaver weaver = new JplPadusWeaver();
		
		/*
		weaver.addAspect("FixedFeeBilling", "/java/workspaces/workspace/padus/demo/Fixed_Billing.xml");
		weaver.addAspect("GenericBilling", "/java/workspaces/workspace/padus/demo/Generic_Billing.xml");
		weaver.addAspect("Logging", "/java/workspaces/workspace/padus/demo/Logging.xml");
		weaver.addProcess("ConferenceCall", "/java/workspaces/workspace/padus/demo/ConferenceCall.bpel");
		weaver.setDeploymentFile("/java/workspaces/workspace/padus/demo/deployment.xml");
//		weaver.setOutputFile("/java/workspaces/workspace/padus/demo/ConferenceCall-new.bpel");
		weaver.setWeaverFile("/java/workspaces/workspace/padus/pl/deployment.pl");
		*/
		weaver.addAspect("FixedFeeBilling", "D:\\workspaces [eclipse]\\Padus\\demo\\Fixed_Billing.xml");
		weaver.addAspect("GenericBilling", "D:\\workspaces [eclipse]\\Padus\\demo\\Generic_Billing.xml");
		weaver.addAspect("Logging", "D:\\workspaces [eclipse]\\Padus\\demo\\Logging.xml");
		weaver.addProcess("ConferenceCall", "D:\\workspaces [eclipse]\\Padus\\demo\\ConferenceCall.bpel");
		weaver.setDeploymentFile("D:\\workspaces [eclipse]\\Padus\\demo\\deployment.xml");
		weaver.setOutputDir("D:\\workspaces [eclipse]\\Padus\\demo\\ConferenceCall-new.bpel");
		weaver.setWeaverFile("D:\\workspaces [eclipse]\\Padus\\pl\\deployment.pl");
		
		weaver.weave();
	}
	
	private <K, V> void configureDefinition(String type, Map<K, V> map) {
		
		List<K> keyList = new ArrayList<K>();
		List<V> valueList = new ArrayList<V>();
		for (Entry<K, V> entry : map.entrySet()) {
			keyList.add(entry.getKey());
			valueList.add(entry.getValue());
		}
		
		Query q = new Query("define" + type,
				new Term[] { getPrologList(keyList), getPrologList(valueList) });
		q.oneSolution();
	}
	
	private <T> Term getPrologList(List<T> list) {
		
		List<Term> terms = new ArrayList<Term>();
		for (T element : list) {
			terms.add(new Atom(element.toString()));
		}
		return Util.termArrayToList(terms.toArray(new Term[] { }));
	}
	
	@Override
	protected void weaveInternal() {
		
		try {
			System.out.print("Weaving...");
			// Save the current System.out stream:
			PrintStream systemOut = System.out;
			// Create a new PrintStream that writes to the output file:
//			PrintStream ps = new PrintStream(new BufferedOutputStream(new FileOutputStream(getOutputFile())), true);
			// Write a test message to the PrintStream:
//			ps.println("test1");
			// Change the System.out stream:
//			System.setOut(ps);
			// Write a test message to the System.out stream:
			System.out.println("test2");
			
			
			
			Query q = new Query("consult",
					new Term[] {
							new Atom(getWeaverFile()) });
			
			if (!q.hasSolution()) {
				throw new RuntimeException("The weaver file named "
						+ getWeaverFile() + " could not be loaded");
			}
			
			configureDefinition("Aspects", getAspects());
			configureDefinition("Processes", getProcesses());	
			
			
			
			(new Query("deploy", new Term[] { new Atom(getDeploymentFile()) })).oneSolution();
			// Flush the System.out stream:
			System.out.flush();
			// Restore the original System.out stream:
			System.setOut(systemOut);
			// Close the PrintStream:
//			ps.close();
			System.out.println("OK");
		} catch (Exception e) {
			System.out.println("FAILED");
			// do nothing
		}
	}
}
