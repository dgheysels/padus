/******************************************************************************
 * Copyright (c) 2006 Vrije Universiteit Brussel
 * All rights reserved.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0, which accompanies this
 * distribution, and is available at http://www.eclipse.org/legal/epl-v10.html.
 * 
 * Contributors:
 *    Niels Joncheere
 *    Mathieu Braem
 ******************************************************************************/
package be.ac.vub.ssel.padus.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;

/**
 * An instance of StreamGobbler keeps reading an input stream, and prints this
 * input to a print stream.  Its main use is for reading the input or error
 * stream of a Runtime.exec() invocation, as input or error buffer overflows may
 * cause such invocations to block (or even deadlock).
 * 
 * @author Niels Joncheere
 * @author Mathieu Braem
 * @see http://www.javaworld.com/javaworld/jw-12-2000/jw-1229-traps.html?page=3
 */
public class StreamGobbler extends Thread {
	
	private InputStream is;
	private PrintStream ps;
	
	public StreamGobbler(InputStream is, PrintStream ps) {
		
		this.is = is;
		this.ps = ps;
	}
	
	public void run() {
		
		try {
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String line;
			while ((line = br.readLine()) != null) {
				ps.println(line);
			}
		} catch (Exception e) {
			//e.printStackTrace();
		}
	}
}
