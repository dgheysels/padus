/******************************************************************************
 * Copyright (c) 2006 Vrije Universiteit Brussel
 * All rights reserved.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0, which accompanies this
 * distribution, and is available at http://www.eclipse.org/legal/epl-v10.html.
 * 
 * Contributors:
 *    Niels Joncheere
 *    Mathieu Braem
 ******************************************************************************/
package be.ac.vub.ssel.padus.util;

import java.io.OutputStream;

public class NullOutputStream extends OutputStream {
	
	@Override
	public void close() {
		
		// do nothing
	}
	
	@Override
	public void flush() {
		
		// do nothing
	}
	
	@Override
	public void write(byte[] b) {
		
		// do nothing
	}
	
	@Override
	public void write(byte[] b, int off, int len) {
		
		// do nothing
	}
	
	@Override
	public void write(int b) {
		
		// do nothing
	}
}
