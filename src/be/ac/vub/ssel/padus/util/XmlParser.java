/******************************************************************************
 * Copyright (c) 2006 Vrije Universiteit Brussel
 * All rights reserved.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0, which accompanies this
 * distribution, and is available at http://www.eclipse.org/legal/epl-v10.html.
 * 
 * Contributors:
 *    Niels Joncheere
 *    Mathieu Braem
 ******************************************************************************/
package be.ac.vub.ssel.padus.util;

import java.io.File;

import org.apache.xerces.parsers.DOMParser;
import org.w3c.dom.Document;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXParseException;

/**
 * @author Niels Joncheere
 * @author Mathieu Braem
 */
public class XmlParser implements ErrorHandler {
	
	private static XmlParser instance;
	
	public static XmlParser getInstance() {
		
		if (instance == null) {
			instance = new XmlParser();
		}
		return instance;
	}
	
	/**
	 * The constructor
	 * 
	 * The visibility of the constructor is private in order to prevent
	 * instantiation.
	 */
	private XmlParser() {
		
		// do nothing
	}
	
	public void error(SAXParseException e) throws SAXParseException {
		
		// do nothing
	}
	
	public void fatalError(SAXParseException e) throws SAXParseException {
		
		// do nothing
	}
	
	public Document parse(File xmlFile) throws SAXParseException {
		
		return parse(xmlFile.getAbsolutePath());
	}
	
	public Document parse(String xmlFile) {
		
		try {
			DOMParser parser = new DOMParser();
			parser.setErrorHandler(this);
			parser.setFeature("http://apache.org/xml/features/validation/schema", true);
			parser.setFeature("http://apache.org/xml/features/validation/schema-full-checking", true);
			parser.setFeature("http://xml.org/sax/features/namespaces", true);
			parser.setFeature("http://xml.org/sax/features/validation", true);
			parser.parse(xmlFile);
			return parser.getDocument();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("The XML file named " + xmlFile + " could not be parsed.");
		}
	}
	
	public void warning(SAXParseException e) throws SAXParseException {
		
		// do nothing
	}
}
