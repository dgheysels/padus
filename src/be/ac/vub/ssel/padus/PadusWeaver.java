/******************************************************************************
 * Copyright (c) 2006 Vrije Universiteit Brussel
 * All rights reserved.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0, which accompanies this
 * distribution, and is available at http://www.eclipse.org/legal/epl-v10.html.
 * 
 * Contributors:
 *    Niels Joncheere
 *    Mathieu Braem
 ******************************************************************************/
package be.ac.vub.ssel.padus;

import be.ac.vub.ssel.padus.util.XmlParser;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Hashtable;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.w3c.dom.Document;

/**
 * @author Niels Joncheere
 * @author Mathieu Braem
 */
public abstract class PadusWeaver {
	
	private Map<String, String> aspects		= new Hashtable<String, String>();
	private Map<String, String> processes	= new Hashtable<String, String>();
	
	private String deploymentFile;
	private String outputDir;
	private String weaverFile;
	
	public void addAspect(String name, File file) {
		
		addAspect(name, file.getAbsolutePath());
	}
	
	/**
	 * Add a file to the list of aspects
	 */
	public void addAspect(String name, String file) {
		
		aspects.put(name, file);
	}
	
	public void addProcess(String name, File file) {
		
		addProcess(name, file.getAbsolutePath());
	}
	
	/**
	 * Add a file to the list of procesess
	 */
	public void addProcess(String name, String file) {
		
		processes.put(name, file);
	}
	
	/**
	 * Clean the resulting output file, by overwriting it with an indented XML tree.
	 * @param file
	 */
	public void cleanFile(String file) {
		
		if (file.length() > 0) {
			try {
				System.out.print("Cleaning...");
				Document bpelDocument = XmlParser.getInstance().parse(file);
				Document outputDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
				outputDocument.appendChild(outputDocument.importNode(bpelDocument.getFirstChild(), true));
				OutputFormat of = new OutputFormat(outputDocument);
				of.setIndenting(true);
				FileOutputStream fos = new FileOutputStream(file);
				XMLSerializer serializer = new XMLSerializer(fos, of);
				serializer.setNamespaces(true);
				serializer.serialize(outputDocument);
				System.out.println("OK");
			} catch (Exception e) {
				System.out.println("FAILED");
				e.printStackTrace();
				throw new RuntimeException("The BPEL file named " + file + " could not be cleaned.");
			}
		} else {
			throw new RuntimeException("The BPEL file named " + file + " could not be woven.");
		}
	}
	
	protected void debug() {
		
		String className = this.getClass().getSimpleName();
		System.out.println(className);
		for (int i = 0; i < className.length(); i++) {
			System.out.print("-");
		}
		System.out.println();
		System.out.println();
		
		printTable("Aspects:", getAspects());
		printTable("Processes:", getProcesses());
		
		System.out.println("  Deployment file:");
		System.out.println("  - " + getDeploymentFile());
		System.out.println();
		
		System.out.println("  Output directory:");
		System.out.println("  - " + getOutputDir());
		System.out.println();
		
		System.out.println("  Weaver file:");
		System.out.println("  - " + getWeaverFile());
		System.out.println();
	}
	
	protected Map<String, String> getAspects() {
		
		return aspects;
	}
	
	protected String getDeploymentFile() {
		
		return deploymentFile;
	}
	
	protected String getOutputDir() {
		
		return outputDir;
	}
	
	protected Map<String, String> getProcesses() {
		
		return processes;
	}
	
	protected String getWeaverFile() {
		
		return weaverFile;
	}
	
	private <K, V> void printTable(String title, Map<K, V> map) {
		
		System.out.println("  " + title);
		int maxLength = 0;
		for (K key : map.keySet()) {
			String string = key.toString();
			int length = string.length();
			if (length > maxLength) {
				maxLength = length;
			}
		}
		for (Entry<K, V> entry : map.entrySet()) {
			K key = entry.getKey();
			String string = key.toString();
			int length = string.length();
			int padding = maxLength - length;
			System.out.print("  - " + key.toString());
			for (int i = 0; i < padding; i++) {
				System.out.print(" ");
			}
			System.out.println(" " + entry.getValue().toString());
		}
		System.out.println();
	}
	
	public void setDeploymentFile(File deploymentFile) {
		
		setDeploymentFile(deploymentFile.getAbsolutePath());
	}
	
	/**
	 * Sets the weaving deployment description.
	 */
	public void setDeploymentFile(String deploymentFile) {
		
		this.deploymentFile = deploymentFile;
	}
	
	public void setOutputDir(File outputDir) {
		
		setOutputDir(outputDir.getAbsolutePath());
	}
	
	public void setOutputDir(String outputDir) {
		
		this.outputDir = outputDir;
	}
	
	public void setWeaverFile(File weaverFile) {
		
		setWeaverFile(weaverFile.getAbsolutePath());
	}
	
	public void setWeaverFile(String weaverFile) {
		
		this.weaverFile = weaverFile;
	}
	
	/**
	 * Invoke the weaver.
	 * The working directory is the location of the Prolog implementation.
	 * All added files need an absolute path (or relative to the working directory). 
	 */
	public void weave() {
		
		debug();
		weaveInternal();
		for (String processName : processes.keySet()) {
			cleanFile(getOutputDir() + processName + "$weaved.xml");
		}
	}
	
	protected abstract void weaveInternal();
}
