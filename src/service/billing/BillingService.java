package service.billing;

import java.io.FileWriter;
import java.io.IOException;

public class BillingService {
	
	private static final float PRICE_TIMEUNIT = 0.01f; 
	
	public BillingService() {}
	
	public void calculatePrice(float time) {
		
		float t = time * PRICE_TIMEUNIT;
		FileWriter billing = null;
		
		try {
			billing = new FileWriter("h:\\billing.txt", false);
			billing.write(Float.toString(t));
			billing.flush();
		}
		catch(IOException ex) {
			ex.printStackTrace();
		}
		finally {
			try {
				billing.close();
			}
			catch(IOException ex) {
				ex.printStackTrace();
			}
		}
	}
	
}
