package service.flight;

import java.util.HashSet;
import java.util.Vector;

public class FlightService {

	private HashSet<Flight> _flights;
	
	public FlightService() {
		_flights = new HashSet<Flight>();
		
		Flight f1 = new Flight("AAA001", "Brussel", "BELGIUM", "Dublin", "IRELAND", "21/10/2006", 150, 150.00f);
		Flight f2 = new Flight("AAA002", "Brussel", "BELGIUM", "Barcelona", "SPAIN", "25/10/2006", 250, 250.00f);
		Flight f3 = new Flight("AAA003", "Charleroi", "BELGIUM", "Paris", "FRANCE", "23/10/2006", 350, 125.00f);
		Flight f4 = new Flight("AAA004", "Oostende", "BELGIUM", "Dublin", "IRELAND", "18/10/2006", 0, 100.00f);
		Flight f5 = new Flight("AAA005", "Brussel", "BELGIUM", "London", "UNITED KINGDOM", "21/10/2006", 1, 150.00f);
		
		_flights.add(f1);
		_flights.add(f2);
		_flights.add(f3);
		_flights.add(f4);
		_flights.add(f5);
		
	}
	
	public Flight[] getFlights(String sourceCountry, String destinationCity, String destinationCountry) {
		
		Vector<Flight> result = new Vector<Flight>();
		
		for (Flight f : _flights) {
			if (f._sourceCountry.equalsIgnoreCase(sourceCountry) &&
				f._destinationCity.equalsIgnoreCase(destinationCity) &&
				f._destinationCountry.equalsIgnoreCase(destinationCountry)) {
				
				result.add(f);
			}
		}
	
		return result.toArray(new Flight[result.size()]);		
	}
	
	/* books the flight -> decrease #availablePlaces
	 * result: ok = true
	 * 		   error = false
	 */
	public boolean bookFlight(String flightNumber) {
		for (Flight f : _flights) {
			if (f._flightNumber.equalsIgnoreCase(flightNumber)) {
				if (f._availablePlaces > 0) {
					f._availablePlaces--;
					return true;
				}
				else {
					return false;
				}
			}
		}
		
		return false;
	}	
}
