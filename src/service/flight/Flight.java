package service.flight;

public class Flight {

	public String _flightNumber;
	public String _sourceCity;
	public String _sourceCountry;
	public String _destinationCity;
	public String _destinationCountry;
	public String _departureDate;
	public int    _availablePlaces;
	public float  _price;
	
	public Flight(String flightNumber, String sourceCity, String sourceCountry, 
					String destinationCity, String destinationCountry, 
					String departureDate, int availablePlaces, float price) {
		
		_flightNumber = flightNumber;
		_sourceCity = sourceCity;
		_sourceCountry = sourceCountry;
		_destinationCity = destinationCity;
		_destinationCountry = destinationCountry;
		_departureDate = departureDate;
		_availablePlaces = availablePlaces;
		_price = price;
		
	}
	
	public String toString() {
		return _flightNumber + "(" + _departureDate + ") : " + 
				_sourceCity + " (" + _sourceCountry + ") -> " + 
				_destinationCity + "(" + _destinationCountry + ") >> " + 
				_price;
	}
}
