package service.hotel;

import java.util.HashSet;
import java.util.Vector;

public class HotelService {

	private HashSet<Hotel> _hotels;
	
	public HotelService() {
		_hotels = new HashSet<Hotel>();
		
		Hotel h1 = new Hotel("Temple Bar Hotel", "Dublin", "IRELAND", 0, 59.0f, 1);
		Hotel h2 = new Hotel("Jurys Croke Park Hotel", "Dublin", "IRELAND", 40, 109.0f, 4);
		Hotel h3 = new Hotel("Camdan Court Hotel", "Dublin", "IRELAND", 246, 89.0f, 3);
		Hotel h4 = new Hotel("Park Plaza Tyrrelstown", "Dublin", "IRELAND", 155, 159.0f, 4);
		Hotel h5 = new Hotel("Olivia Plaza Hotel", "Barcelona", "SPAIN", 113, 100.0f, 4);
		Hotel h6 = new Hotel("Catalunya Plaza", "Barcelona", "SPAIN", 80, 119.0f, 3);
		Hotel h7 = new Hotel("The Howard London", "London", "UNITED KINGDOM", 189, 189.0f, 5);
		
		_hotels.add(h1);
		_hotels.add(h2);
		_hotels.add(h3);
		_hotels.add(h4);
		_hotels.add(h5);
		_hotels.add(h6);
		_hotels.add(h7);
	}
	
	/* Hotels located in 'city' which have some rooms available */
	public Hotel[] getAvailableHotels(String city) {
		
		Vector<Hotel> result = new Vector<Hotel>();
		
		for (Hotel h : _hotels) {
			if ( (h._city.equalsIgnoreCase(city)) && (h._roomsAvailable > 0)) {
				result.add(h);
			}
		}
		
		return result.toArray(new Hotel[result.size()]);		 
	}
	
	/* get hotel by name */
	public Hotel getHotel(String name) {
		
		for (Hotel h : _hotels) {
			if (h._name.equalsIgnoreCase(name)) {
				return h;
			}
		}
		
		return null;
	}
	
	/* book hotel: decrease number of available rooms
	 * result: true = ok
	 * 		   false = error
	 */
	public boolean bookHotel(String hotelName) {
		for (Hotel h : _hotels) {
			if ( h._name.equalsIgnoreCase(hotelName)) {
					h._roomsAvailable--;
					return true;
			}
		}
		
		return false;
	}	
}
