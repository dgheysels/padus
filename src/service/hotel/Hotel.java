package service.hotel;

public class Hotel {

	public String _name;
	public String _city;
	public String _country;
	public int    _roomsAvailable;
	public float  _price;  // price / dag
	public int    _rating;
	
	public Hotel(String name, String city, String country, int numberOfRooms, float price, int rating) {
		_name = name;
		_city = city;
		_country = country;
		_roomsAvailable = numberOfRooms;
		_price = price;
		_rating = rating;
	}
}
