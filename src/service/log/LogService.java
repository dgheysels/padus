package service.log;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LogService {

	private SimpleDateFormat _df = new SimpleDateFormat("d-M-y H'u'm'm's's'");
	
	public LogService() {}

	public void log(String text) {
		
		FileWriter logger = null;
		
		try {
			logger = new FileWriter("c:\\logBPEL.txt", true);
			logger.write(_df.format(new Date()) + " > " + text + "\n");
			logger.flush();
		}
		catch(IOException ex) {
			ex.printStackTrace();
		}
		finally {
			try {
				logger.close();
			}
			catch(IOException ex) {
				ex.printStackTrace();
			}
		}
	}
}
