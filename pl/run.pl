%% Emacs, this is -*-prolog-*-

:- consult('xml-facts-mapping').
:- consult('pc-lib').
:- consult('advisor').

clear :- clear(_).
clear(Type) :- clear_base(Type), clear_advice(Type).

run :-
    clear,
    loadAndParse('xml/ConfCallSetup.xml'),
    write_ln('okidoki'),
    generateAndPrint.

tst_after :-
    invoking(Jp, [operation(check)]),
    after(Jp, 'Old_XML/tst-advice.xml').

tst_around :-
    pc_(Jp, assign, []),
    around(Jp, 'Old_XML/tst-advice-proceed.xml').

tst_in :-
    pc_(Jp, flow, []),
    in(Jp, 'Old_XML/tst-advice.xml').

tst_advice_call :-
    loadAndParseNext('Old_XML/tst-advice-called.xml', _),
    pc_(Jp, flow, []),
    after(Jp, 'Old_XML/tst-advice-caller.xml').
    
tst :-
    clear, loadAndParse('Old_XML/tst-bpel.xml'),
    tst_after, tst_around, tst_in, tst_advice_call,
    tst_around,
    generateAndPrint.

