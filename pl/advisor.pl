%% Emacs, this is -*-prolog-*-

:- consult('xml-facts-mapping'). 

:- dynamic after_/3.
:- dynamic after_ns/3.

:- dynamic before_/3.
:- dynamic before_ns/3.

:- dynamic around_/3. 
:- dynamic in_/3. 


clear_advice :-
	clear_advice(_).

clear_advice(Type) :-
    retractall(after_(_,_,Type)),
    retractall(after_ns(_,_,Type)),

    retractall(before_(_,_,Type)),
    retractall(before_ns(_,_,Type)),

    retractall(around_(_,_,Type)),
    retractall(in_(_,_,Type)).


%% Predicates to advise joinpoints.
%% Parse the XML in the file and keep a link to its root's ID.

after( Jp, AdviceId) :- after( Jp, AdviceId, 0).
after( Jp, AdviceId, Type) :- assert(after_( Jp, AdviceId, Type)).
afterNs( Jp, AdviceId, Type) :- myprint('asserted','ADVISOR'), assert(after_ns( Jp, AdviceId, Type)).

before(Jp, AdviceId) :- before(Jp, AdviceId, 0).  
before(Jp, AdviceId, Type) :- assert(before_( Jp, AdviceId, Type)).
beforeNs(Jp, AdviceId, Type) :- assert(before_ns( Jp, AdviceId, Type)).

around( Jp, AdviceId) :-around( Jp, AdviceId, 0). 
around( Jp, AdviceId, Type) :- assert(around_( Jp, AdviceId, Type)).

in( Jp, AdviceId) :- in( Jp, AdviceId, 0). 
in( Jp, AdviceId, Type) :- assert(in_( Jp, AdviceId, Type)).



advised(Id, before, Advice, Type) :- before_(Id, Advice, Type).
advised(Id, before_ns, Advice, Type) :- before_ns(Id, Advice, Type).

advised(Id, after, Advice, Type) :- after_(Id, Advice, Type).
advised(Id, after_ns, Advice, Type) :- after_ns(Id, Advice, Type).

advised(Id, around, Advice, Type) :- around_(Id, Advice, Type).
advised(Id, in, Advice, Type) :- in_(Id, Advice, Type).


%% Tests if a node is advised 
is_advised(_, [], _) :- fail. 
is_advised(Id, T, Type) :- advised(Id, T, _, Type). 
is_advised(Id, [T|Types], Type) :-
    is_advised(Id, T, Type); is_advised(Id, Types, Type).

is_advised(Id, Type) :-
    is_advised(Id, [before, before_ns, after, after_ns, around, in], Type).


%% Finds the node that is advised with this proceed
proceeds(Advice, Base, Type) :- 
    around_(Base, Advice, Type).

proceeds(Advice, Base, Type) :- 
    child(Parent, Advice, _, Type),
    Parent \= Advice, % At advice root node, no target in base code found
    proceeds(Parent, Base, Type).


%% Finds the node that is called with this advice call
%adviceCalls(Caller, Advice, Type) :-
%    attribute(Caller, name, Name, Type),
%    attribute(Advice, name, Name, Type),
%    Caller \= Advice, myprint('adviceCalls', Caller, Advice, 'ADVISOR'),
%    node(Advice, advice, Type).


%% Maximum number in a list
%% Bestaat hier ook een std lib predikaat voor?
maximum([X|Xs],M) :- maximum(Xs,M,X).
maximum([],M,M).
maximum([X|Xs],M,A) :- X > A, maximum(Xs,M,X).
maximum([X|Xs],M,A) :- X =< A, maximum(Xs,M,A).

