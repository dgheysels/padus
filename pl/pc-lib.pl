%% Emacs, this is -*-prolog-*-

%% Predicates for selecting "joinpoints" in the system

pc_(Jp, Activity, []) :-
    node(Jp, NSActivity, 'process'),
    split(NSActivity, _, ':', Activity),
    (maxProcessId(MaxId,'process'), Jp<MaxId).

pc_(Jp, Activity, [Attr | Attributes]) :- 
    Attr =.. [Key, Value],
    attribute(Jp, Key, NSValue, 'process'),
    split(NSValue, _, ':', Value),
    pc_(Jp, Activity, Attributes).

%% Pointcut predicates
invoking(Jp) :- invoking(Jp, []).
invoking(Jp, Attrs) :- pc_(Jp, invoke, Attrs).

invoking(Jp, Name, PartnerLink, PortType, Operation, InputVariable, OutputVariable) :-
	invoking(Jp, [name(Name),partnerLink(PartnerLink),portType(PortType),operation(Operation),inputVariable(InputVariable),oupuntVariable(OutputVariable)]).
invoking(Jp, Name, PartnerLink, PortType, Operation) :-
	invoking(Jp, [name(Name),partnerLink(PartnerLink),portType(PortType),operation(Operation)]).
invoking(Jp, PartnerLink, PortType, Operation) :- 
	invoking(Jp, [partnerLink(PartnerLink),portType(PortType),operation(Operation)]).

	
receiving(Jp) :- receiving(Jp, []).
receiving(Jp, Attrs) :- pc_(Jp, receive, Attrs).

replying(Jp) :- replying(Jp, []).
replying(Jp, Attrs) :- pc_(Jp, reply, Attrs).

assigning(Jp) :- assigning(Jp, []).
assigning(Jp, Attrs) :- pc_(Jp, assign, Attrs).

scoping(Jp) :- scoping(Jp, []).
scoping(Jp, Attrs) :-  pc_(Jp, scope, Attrs).

picking(Jp) :- picking(Jp, []).
picking(Jp, Attrs) :- pc_(Jp, pick, Attrs).

switching(Jp) :- switching(Jp, []).
switching(Jp, Attrs) :- pc_(Jp, switch, Attrs).

flowing(Jp) :- flowing(Jp, []).
flowing(Jp, Attrs) :- pc_(Jp, flow, Attrs).

looping(Jp) :- looping(Jp, []).
looping(Jp, Attrs) :- pc_(Jp, while, Attrs).
	  
%...




% Supplementary predicates

% Links a joinpoint with the process it is defined in.
inProcess(Joinpoint, Attrs):- pc_(Jp, process, Attrs).

% Links a joinpoint with the process instance it occurs in.
inProcessInstance(Joinpoint, ProcessInstance):- write_ln('Not implemented yet!').

% Links the name of a variable to its value in a specific process instance.
variableValue(ProcessInstance, Name, Value):- write_ln('Not implemented yet!').

variableValue(ProcessInstance, Name, Value, Advice, [element(switch, [], [element(case, [condition=Condition], [Advice])])]) :-
	concat('getVariableData(\'',Name,A),
	concat(A,'\')=\'',B),
	concat(B, Value, C),
	concat(C, '\'', Condition).




