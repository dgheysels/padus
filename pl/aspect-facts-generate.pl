%% Emacs, this is -*-prolog-*-
	
test2 :-
	clear,

	%% Demo Purpose
%%	myprint('********************', 'INFO'), myprint('* ORIGINAL ASPECT: ','*','INFO'), myprint('********************', 'INFO'),
%%	loadAndParseFile('xml/Fixed_Billing.xml','aspect'),
%%	generateAndPrint('aspect'),
%%	clear,
%%	myprint('**********************', 'INFO'), myprint('* DEPENDENCY ASPECT: ','*','INFO'), myprint('**********************', 'INFO'),
%%	loadAndParseFile('xml/Generic_Billing.xml','aspect'),
%%	generateAndPrint('aspect'),
	
	clear,clear_advices,
	loadAndParseFile('demo/Fixed_Billing.xml','aspect'),
%	loadAndParseFile('demo/Logging.xml','aspect'),
	defineAspects(['FixedFeeBilling'|'GenericBilling'],['demo/Fixed_Billing.xml'|'demo/Generic_Billing.xml']),
%	defineAspects(['Logging'],['demo/Logging.xml']),
	iterateOverIncludes,
	iterateOverAdviceConstructs,
	myprint('*********************', 'INFO'), myprint('* RESULTING ASPECT: ','*','INFO'), myprint('*********************', 'INFO'),
	generateAndPrint('aspect').  

clear_advices :-
	myprint('Clearing Advices ...','ASPECTS'),
	clause(advices(A),Head),!,
	forall(advices(T1), retract((T1 :- T2))),
	retractall(advices(_)).

   
clear_advices :- myprint('No Advices Found...','ASPECTS').


  
%%  INCLUSION  
%% ------------

%TODO
% - node to aspect node
% - group namespaces, using, ...

%% Get Inclusion Tags from Aspect Structure
iterateOverIncludes :-
	findall(IncludeNodeId, (node(IncludeNodeId, NSAspect,'aspect'), split(NSAspect, _, ':','include') ),
		IncludeNodeIds), myprint('IncludeNodeIds: ', IncludeNodeIds,'ASPECTS'),
	loadInclusionInAspectStructure(IncludeNodeIds),
	reGenerateAndParseStructure('aspect').

loadInclusionInAspectStructure([IncludeNodeId | Siblings]) :-
	attribute(IncludeNodeId, 'name', Value, 'aspect'),
	aspect(Value,File),
	myprint('Get file from aspect definition: ',Value,File,'ASPECTS'),
	generateAspectWithIncludedContent(IncludeNodeId,File),
	loadInclusionInAspectStructure(Siblings).

loadInclusionInAspectStructure([]).



%%   GENERATE NEW ASPECT WITH INCLUDED CONTENT
%% --------------------------------------------

%% Use Around Advice (without proceed) and after advice to include new content

generateAspectWithIncludedContent( Jp, File) :-
	loadFileInStructure( File, [element( Tag, [NameAttr | Atts], Content)]),
	generateAspectWithIncludedNameSpaces(Atts),
	generateAspectWithIncludedBody( Jp, [element( Tag, [NameAttr | Atts], Content)]).
  

% Include new Namespaces ( xmlns:pred = predValue => compare )
generateAspectWithIncludedNameSpaces([Attr | Atts]) :-
       	myprint('Attributes: ', Attr, 'ASPECTS'),
	findall( Key = Value, attribute( 0, Key, Value, 'aspect'), Attributes),
	addNameSpace( Attr, Attributes),
	generateAspectWithIncludedNameSpaces(Atts).

generateAspectWithIncludedNameSpaces( []).

addNameSpace(Attr, Attributes):-
	\+ member(Attr, Attributes), !, myprint('okay','ASPECTS'), assert_properties(0, [Attr],'aspect').

addNameSpace( _, _).

% Include new Body
generateAspectWithIncludedBody( Jp, [element( NS, _Atts, [Child | Children])]) :- myprint('Children1: ', Children,'ASPECTS'),
        parseAndLinkBodyElements( Jp, Children),
	parseNextStructure( [Child], ContentToIncludeId, 'aspect'),
	around(Jp, ContentToIncludeId, 'aspect').


parseAndLinkBodyElements(Jp, []).

parseAndLinkBodyElements( Jp, [Element | Siblings]) :- 
	parseNextStructure( [Element], ContentToIncludeId, 'aspect'),myprint('Element', Element,'ASPECTS'),
	myprint('ids: ', Jp, ContentToIncludeId,'ASPECTS'),
	afterNs(Jp, ContentToIncludeId, 'aspect'),
	parseAndLinkBodyElements( Jp, Siblings).


%%  ADVICE CONSTRUCT  
%% ------------------

iterateOverAdviceConstructs :-
	findall(AdviceNodeId, ( node(AdviceNodeId, NSAspect,'aspect'), split(NSAspect, _, ':','advice') ),
				AdviceNodeIds ), myprint('AdviceNodeIds: ', AdviceNodeIds,'ASPECTS'),
	findall(AdviceNodeIdWithoutChildren, ( member(AdviceNodeIdWithoutChildren, AdviceNodeIds), \+ child( AdviceNodeIdWithoutChildren, ChildId, Order,'aspect')),AdviceNodeIdsWithoutChildren), myprint('AdviceNodeIdsWithoutChildren: ', AdviceNodeIdsWithoutChildren,'ASPECTS'),
	findall(AdviceNodeIdWithChildren, ( member(AdviceNodeIdWithChildren, AdviceNodeIds), Child^child( AdviceNodeIdWithChildren, ChildId, Order,'aspect'),!),	AdviceNodeIdsWithChildren), myprint('AdviceNodeIdsWithChildren: ', AdviceNodeIdsWithChildren,'ASPECTS'),
	assertAdvices(AdviceNodeIdsWithChildren),
	replaceAdviceInAspectStructure(AdviceNodeIdsWithoutChildren),
	reGenerateAndParseStructure('aspect').

replaceAdviceInAspectStructure([AdviceNodeIdWithoutChildren | Siblings]) :-
	generateAdviceWithIncludedBody(AdviceNodeIdWithoutChildren),
	replaceAdviceInAspectStructure(Siblings).

replaceAdviceInAspectStructure([]).


generateAdviceWithIncludedBody(AdviceNodeIdWithoutChildren):-
	%findall(Child, child(CorrespondingAdviceNodeId, Child, Order, 'aspect'), [ ChildId | Children]),
	attribute(AdviceNodeIdWithoutChildren, 'name', Value, 'aspect'),
	atom_to_term(Value, T1, B1), T1 =.. List, append(List, Children, T2), flatten(T2,T3), T4 =.. T3, append(B1, Children, B2), flatten(B2,B3),
	T4,=(Children,[ChildId|Siblings]),
	linkBodyElements(AdviceNodeIdWithoutChildren, Siblings), myprint('Children: ', ChildId, Siblings,'ASPECTS'),
	parseNextStructure([ChildId], NextId, 'aspect'),
	around(AdviceNodeIdWithoutChildren, NextId, 'aspect').

linkBodyElements(Jp, []).
        
linkBodyElements(Jp, [Element | Siblings]) :-
	parseNextStructure([Element], NextId, 'aspect'),
	afterNs(Jp, NextId, 'aspect'),
	linkBodyElements(Jp,Siblings).

assertAdvices([AdviceNodeIdWithChildren | AdviceNodeIdsWithChildren]):-
	attribute(AdviceNodeIdWithChildren, 'name', Name, 'aspect'),
	myprint('Name: ', Name, 'ASPECTS'),
%	correctQuotes(Name, NameResult),
%	myprint('AdviceName: ', NameResult,'ASPECTS'),
	atom_to_term( Name, T1, B1), T1 =.. List, append(List, Children, T2), flatten(T2,T3), T4 =.. T3, append(B1, Children, B2), flatten(B2,B3),
	assert( ( T4 :- adjustAdvice(AdviceNodeIdWithChildren,B3,Children) )), assert(advices(T4)), myprint('Asserted!!!!! ', 'ASPECTS'), 	
	assertAdvices(AdviceNodeIdsWithChildren).
	
assertAdvices([]).

%% Adjust Advice Content to parameterValues
adjustAdvice(AdviceNodeIdWithChildren, Arguments, NewAdvice) :-
	remove_last_element(Arguments, ParameterValues), myprint('ParameterValues: ',ParameterValues, 'ASPECTS'),
	generateWithParameters(NewAdvice, AdviceNodeIdWithChildren, ParameterValues, 'aspect'), myprint('NewAdvice: ',NewAdvice, 'ASPECTS').

remove_last_element([], _) :- fail.
remove_last_element([X], []).
remove_last_element([X,Y|Z], [X|T]) :-
    remove_last_element([Y|Z],T).

%% First and last quotes must be removed for assertion (nested quotes)
correctQuotes(String, Result):-
	split(String,A,'\'',B), correctQuotes(B, CorrectB),
	concat(A,'\'',C),
	concat(C, CorrectB, Result).
	
correctQuotes(String, String).
%*****************************************************
%* Clean up space , generate and parse new structure *
%*****************************************************

reGenerateAndParseStructure(Type) :-
	generate(Structure, Type),
	clear_base(Type),
	clear_advice(Type),
	parse(Structure, 0, Type).


