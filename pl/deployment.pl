%% Emacs, this is -*-prolog-*-

:- dynamic aspect/2.
:- dynamic process/2.

:- consult('xml-facts-mapping').
:- consult('xml-facts-parse').
:- consult('xml-facts-generate').

:- consult('aspect-facts-generate').
:- consult('aspect-to-process').
:- consult('pc-lib').
:- consult('advisor').
:- consult('run').
:- consult('stateful').

test1a :-
        clear,
        defineAspects(['FixedFeeBilling','GenericBilling','Logging'],['demo/Fixed_Billing.xml','demo/Generic_Billing.xml','demo/Logging.xml']),
        defineProcesses(['ConferenceCall'],['demo/ConferenceCall.bpel']),
   %     setOutputDir('e:/workspace/padus/demo/'),
        deploy('demo/deployment.xml').

test1b :-
        clear,
        defineAspects(['Logging'],['/windows/D/workspace/padus/demo/Logging.xml']),
        defineProcesses(['ConferenceCall'],['/windows/D/workspace/padus/demo/ConferenceCall.bpel']),
    %    setOutputDir('/windows/D/workspace/padus/demo/'),
        deploy('/windows/D/workspace/padus/demo/deployment.xml').

test1c :-
        clear,
        defineAspects(['FSA'],['demo/FSA.xml']),
       % defineProcesses(['Demonstrator'],['/windows/D/workspace/padus/demo/Demonstrator.bpel']),
        setOutputDir('demo/'),
        deploy('/windows/D/workspace/padus/demo/deployment_3.xml').



%%  INITIALISATION
%% ----------------

clear :- clear(_).
clear(Type) :- clear_base(Type), clear_advice(Type), clear_pointcuts, clear_advices.
clearOutputDir :- retractall(outputDir(_)).

%% Associate an aspect with an xml file 
defineAspects(Aliases,Files) :-
        assert_aspects(Aliases,Files).

assert_aspects([],[]).
assert_aspects([Alias|Aliases],[File|Files]):-
        assert(aspect(Alias, File)),
        myprint( 'Aspect Defenitions: ', [Alias|Aliases], [File|Files], 'DEPLOYMENT'),
        flatten(Aliases, Aa),
        flatten(Files, Bb),
        assert_aspects(Aa, Bb).
%assert_aspects(_,_).



%% Associate a process with an xml file
defineProcesses(Aliases,Files) :-
        assert_processes(Aliases,Files).

assert_processes([],[]).
assert_processes([Alias|Aliases],[File|Files]):-
        assert(process(Alias,File)),
        myprint('Process Defenitions: ',[Alias|Aliases],[File|Files], 'DEPLOYMENT'),
        flatten(Aliases,Aa),
        flatten(Files,Bb),
        assert_processes(Aa,Bb).
%assert_processes(_,_).

setOutputDir(Dir) :-
        assert(outputDir(Dir)).
        
getFile(Value,DestFile):-
	(clause(outputDir(D),Head)-> outputDir(Dir);=(Dir,'')),
        concat(Dir,Value,R1),
        concat(R1,'$weaved.xml',DestFile),
        myprint('Destination File: ', DestFile,'INFO').
        
        

%%  Deployment
%% ------------

deploy(File) :-
        myprint('*********************', 'INFO'), myprint('* Parse Deployment: *', 'INFO'), myprint('*********************', 'INFO'),
        loadAndParseFile(File,'deployment'),
        generateAndPrint('deployment'),
        myprint('Iterating over Processes... ', 'INFO'),
        iterateOverProcesses.

resolveAspectName(AspectNameValue, ProcessValue, IdValue) :-
        findall(Id1, (attribute(Id1,'process', ProcessValue, 'deployment'),
                      attribute(Id2,'id', IdValue, 'deployment'),
                      Id1=Id2,
                      node(Id1, NSAspect, 'deployment'),
                      split(NSAspect, _, ':','aspect')), [AspectNodeId]),
        attribute(AspectNodeId, 'name', AspectNameValue, 'deployment').



%%   PROCESSES
%% ------------
iterateOverProcesses :-
        findall(ProcessNodeId, (child(0, ProcessNodeId, Order, 'deployment'), node(ProcessNodeId,NSPresendence,'deployment'),
                                split(NSPresendence, _, ':','precedence')),ProcessNodeIds),
        myprint('ProcessNodeIds: ', ProcessNodeIds, 'DEPLOYMENT'),
        loadProcessInStructure(ProcessNodeIds),
        clearOutputDir.

%% Get process file from deployment structure and parse to facts
%% ProcessNodeId = Id of 'precedence tag'  that has been processed
loadProcessInStructure([ProcessNodeId | Siblings]) :-
        attribute(ProcessNodeId,'process',Value,'deployment'),
        process(Value,File),
        myprint('Get file from process definition: ',Value,File, 'DEPLOYMENT'),
        myprint('************************', 'INFO'), myprint('* Parse ProcessID: ', ProcessNodeId,'*', 'INFO'),
              myprint('************************', 'INFO'),

        loadAndParseFile(File,'process'),
        generateAndPrint('process'),

        myprint('Iterating over Aspects... ', 'INFO'),
        iterateOverAspects(ProcessNodeId),

        myprint('**********************', 'INFO'), myprint('* Resulting Process: *','INFO'),
              myprint('**********************', 'INFO'),
        generateAndPrint('process'),
        getFile(Value,DestFile),
        generateAndWrite(DestFile),

        clear_base('process'),
        loadProcessInStructure(Siblings).

loadProcessInStructure([]).

%%   ASPECTS
%% ------------
iterateOverAspects(ProcessNodeId):-
        findall(AspectNodeId, (child(ProcessNodeId, AspectNodeId, Order, 'deployment'), node(AspectNodeId, NSAspect,'deployment'),
                               split(NSAspect, _, ':','aspect') ),AspectNodeIds),
        myprint('AspectNodeIds: ', AspectNodeIds, 'DEPLOYMENT'),     
        loadAspectInStructure(AspectNodeIds, ProcessNodeId).


%% Get aspect file from deployment structure
loadAspectInStructure([AspectNodeId | Siblings], ProcessNodeId) :-
        myprint('AspectNodeId ..... ',AspectNodeId, 'DEPLOYMENT'),
        attribute(AspectNodeId,'id',AspectIdValue,'deployment'),
        attribute(ProcessNodeId,'process',ProcessValue,'deployment'),
        resolveAspectName(AspectNameValue,ProcessValue,AspectIdValue),
        aspect(AspectNameValue,File),
        myprint('Aspect Name/File: ',AspectNameValue,File, 'DEPLOYMENT'),
        loadAndParseFile(File,'aspect'),
        myprint('***********************', 'INFO'), myprint('* Parse AspectID: ',AspectNodeId,'*','INFO'), myprint('***********************', 'INFO'),
        generateAndPrint('aspect'),
        iterateOverIncludes,
        iterateOverAdviceConstructs,
        weaveAspectsInProcess,
        weaveUsingStatements,
        myprint('***************************', 'INFO'), myprint('* Resulting AspectID: ',AspectNodeId,'*','INFO'), myprint('***************************', 'INFO'),
        generateAndPrint('aspect'),
        clear_base('aspect'),
        loadAspectInStructure(Siblings, ProcessNodeId).

loadAspectInStructure([],_).


%%   LOAD & PARSE
%% ---------------


%% Load & Parse file
loadAndParseFile(File):-
        loadAndParseFile( File, 'disable').

loadAndParseFile(File,RootTag):-
        loadAndParseFile(File,RootTag, NewStructure).

loadAndParseFile(File,RootTag, NewStructure):-  
        loadFileInStructure(File, Structure),
        myprint('Parsing file ...', 'DEPLOYMENT'),
        myprint('   Parsed structure: ',Structure, 'DEPLOYMENT'),
        loadStructureWithRootTag( RootTag, Structure, NewStructure),
        parse(NewStructure,0,RootTag).


%% Load & Parse Next

%TODO
% - NextId specific for space

loadAndParseNextFile(File, NextId):-
        loadAndParseNextFile(File, NextId, 'disable').
                
loadAndParseNextFile(File, NextId, RootTag):-
        loadAndParseNextFile(File, NextId, RootTag, NewStructure).

loadAndParseNextFile(File, NextId, RootTag, NewStructure):-
        loadFileInStructure(File, Structure),    
        loadStructureWithRootTag( RootTag, Structure, NewStructure),
        parseNextStructure(NewStructure, NextId, RootTag).

parseNextStructure(Structure, NextId, RootTag):-
        maxId(Id, RootTag),
        next(Id, NextId),
        parse(Structure, NextId, RootTag).





% Load Structure according to RootTag

loadStructureWithRootTag(  _, [], NewStructure):-
        Structure = [].
        % write_ln('empty').

loadStructureWithRootTag( RootTag, Structure, NewStructure):-
        RootTag = 'disable', !, NewStructure = Structure.

loadStructureWithRootTag( RootTag, [element( NS, _Atts, Children) | Siblings], NewStructure):-  
        myprint('namespace: ', NS, 'DEPLOYMENT'),
        split( NS, _, ':', RootTag), !,  myprint('found!!!!!!', 'DEPLOYMENT'),
        =(NewStructure, [element( NS, _Atts, Children) | Siblings]).

loadStructureWithRootTag( RootTag, [element( NS, _Atts, Children) | Siblings], NewStructure):-
        %myprint('children: ',Children, 'DEPLOYMENT'),
        %myprint('siblings: ',Siblings, 'DEPLOYMENT'),
        loadStructureWithRootTag( RootTag, Children, NewStructure),
        loadStructureWithRootTag( RootTag, Siblings, NewStructure).

%% Use Prolog library to parse xml-files
loadFileInStructure(File, Structure) :-
        load_structure(File, Structure, [dialect(xml), space(remove)]).

%% Split namespace from actual tag
split(ABC, A, B, C) :-
        concat(A, BC, ABC),
        concat(B, C, BC).

% If no namespace is used
split(ABC, _, ':', ABC) :- not(sub_string(ABC,A,B,C,':')).


myprint(Aa,Bb,Cc,Dd, Type) :-
        logging(STypes),
        member(Type, STypes),!,
        write(Aa),
        write(Bb),
        write(' / '),
        write(Cc),
        write(' / '),
        write_ln(Dd).
myprint(Aa,Bb,Cc,Dd, Type).


myprint(Aa,Bb,Cc, Type) :-
        logging(STypes),
        member(Type, STypes),!,
        write(Aa),
        write(Bb),
        write(' / '),
        write_ln(Cc).
myprint(Aa,Bb,Cc, Type).

myprint(Aa,Bb, Type) :-
        logging(STypes),
        member(Type, STypes),!,
        write(Aa),
        write_ln(Bb).
myprint(Aa,Bb, Type).

myprint(Aa, Type) :-
        logging(STypes),
        member(Type, STypes),!,
        write_ln(Aa).
myprint(Aa, Type).



logging(Types):-
        Types = ['INFO'].

% 'PARSE','GENERATE','ASPECTS','ATOP','DEPLOYMENT'
