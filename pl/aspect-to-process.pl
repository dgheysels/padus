%% Emacs, this is -*-prolog-*-

test3 :-
	clear, clear_pointcuts,


%% LINUX

%	defineAspects(['FixedFeeBilling'|'GenericBilling'],['/windows/D/workspace/padus/demo/Fixed_Billing.xml'|'/windows/D/workspace/padus/demo/Generic_Billing.xml']),
%	defineProcesses(['workflow'],['/windows/D/workspace/padus/demo/demonstrator.bpel']),
	
%	loadAndParseFile('/windows/D/workspace/padus/demo/ConferenceCall.bpel','process'),	
%	loadAndParseFile('/windows/D/workspace/padus/demo/test_in_attribute.xml','aspect'),
%	loadAndParseFile('/windows/D/workspace/padus/demo/Logging.xml','aspect'),
%	loadAndParseFile('/windows/D/workspace/padus/demo/Fixed_Billing_HL.xml','aspect'),

%% WINDOWS

	defineAspects(['FixedFeeBilling'|'GenericBilling'],['e:/workspace/padus/demo/Fixed_Billing_HL.xml'|'e:/workspace/padus/demo/Generic_Billing.xml']),
	defineProcesses(['workflow'],['e:/workspace/padus/demo/demonstrator.bpel']),
	
	loadAndParseFile('e:/workspace/padus/demo/ConferenceCall.bpel','process'),	
%	loadAndParseFile('/windows/D/workspace/padus/demo/test_in_attribute.xml','aspect'),
%	loadAndParseFile('/windows/D/workspace/padus/demo/Logging.xml','aspect'),
	loadAndParseFile('e:/workspace/padus/demo/Fixed_Billing_HL.xml','aspect'),
	
	iterateOverIncludes,
	iterateOverAdviceConstructs,

	weaveAspectsInProcess,
	weaveUsingStatements,
	
	generateAndPrint('aspect'),
	generateAndPrint('process').


clear_pointcuts :-
	myprint('Clearing Pointcuts ...','ATOP'),
	clause(pointcuts(A,B),Head),!,
	forall(pointcuts(T1,T2), retract((T1:-T2))),
	retractall(pointcuts(_,_)).

   
clear_pointcuts :- myprint('No Pointcuts Found...','ATOP').

%%  Apply Aspects to Process  
%% -------------------------

weaveAspectsInProcess :-

    findall(PointcutNodeId, ( node(PointcutNodeId, NSPointcut,'aspect'), (split(NSPointcut, _, ':','pointcut') | NSPointcut = ('pointcut')) )
	       , PointcutNodeIds ),	myprint(' PointcutNodeIds: ', PointcutNodeIds, 'ATOP'),
	
	assertPointcuts(PointcutNodeIds),
	
	assertFSA, 
	
	
	findall(AfterNodeId, ( node(AfterNodeId, NSAfter,'aspect'), (split(NSAfter, _, ':','after') | NSAfter = ('after')) ),AfterNodeIds ),
	     myprint('AfterNodeIds: ', AfterNodeIds,'ATOP'),
	     weaveAfterAdviceInProcess(AfterNodeIds),
	findall(BeforeNodeId, ( node(BeforeNodeId, NSBefore,'aspect'), (split(NSBefore, _, ':','before') | NSBefore = ('before')) ),BeforeNodeIds ),
	     myprint('BeforeNodeIds: ', BeforeNodeIds,'ATOP'),
	     weaveBeforeAdviceInProcess(BeforeNodeIds),
	findall(AroundNodeId, ( node(AroundNodeId, NSAround,'aspect'), (split(NSAround, _, ':','around') | NSAround = ('around')) ),AroundNodeIds ),
	     myprint('AroundNodeIds: ', AroundNodeIds,'ATOP'),
	     weaveAroundAdviceInProcess(AroundNodeIds),
	findall(InNodeId, ( node(InNodeId, NSIn,'aspect'), (split(NSIn, _, ':','in') | NSIn = ('in')) ),InNodeIds ),
	     myprint('InNodeIds: ', InNodeIds,'ATOP'),
	     weaveInAdviceInProcess(InNodeIds).
	%clear_pointcuts.

% AssertPointcuts
% ----------------
assertPointcuts( [PointcutNodeId | PointcutNodeIds]) :-
	attribute(PointcutNodeId, 'name', Name, 'aspect'), 
	attribute(PointcutNodeId, 'pointcut', Pointcut, 'aspect'),
	myprint('Name/Pointcut: ', Name, Pointcut, 'ATOP'),
	
%	correctQuotes(Name, NameResult),
%	myprint('PointcutName: ', NameResult,'ATOP'),
%	atom_to_term( NameResult, T1, B1),
	atom_to_term( Name, T1, B1),
	myprint('OK1o: ', 'ATOP'),
	
%	correctQuotes(Pointcut, PointcutResult),
%	myprint('PointcutBody: ', PointcutResult, 'ATOP'),
%	atom_to_term(PointcutResult, T2, B1),
	atom_to_term(Pointcut, T2, B1),
	myprint('OK2: ', T2, 'ATOP'),
	myprint('OK3: ', B1, 'ATOP'),
	
	assert( ( T1 :- T2 ) ), 
	assert(pointcuts(T1,T2)), myprint('Asserted!!!!! ', 'ATOP'), 	
	
	assertPointcuts(PointcutNodeIds).


assertPointcuts([]).

%% First and last quotes must be removed for assertion (nested quotes)
%correctQuotes(String, Result):-
%	split(String,A,'\'',B), correctQuotes(B, CorrectB),
%	concat(A,'\'',C),
%	concat(C, CorrectB, Result).
	
%correctQuotes(String, String).

% After Advice
% -------------
weaveAfterAdviceInProcess([AfterNodeId | AfterNodeIds]) :-
	generate( Structure, AfterNodeId, 'aspect'),
	getPointcutsAndAdvice(Structure, PointcutList, Advice),	myprint('PoincutList/Advice : ', PointcutList, Advice, 'ATOP'),
	
	separateProtocolsFromPointcuts(PointcutList, Protocols, Pointcuts), %% ADDED
		
	applyPointcuts(Pointcuts, Advice, 'after'),						%% CHANGED PointcutList = Pointcuts
	
	applyProtocols(Protocols, Advice, 'after'),						%% ADDED
	
	weaveAfterAdviceInProcess(AfterNodeIds).
weaveAfterAdviceInProcess([]). 

% Before Advice
% -------------
weaveBeforeAdviceInProcess([BeforeNodeId | BeforeNodeIds]) :-
	generate( Structure, BeforeNodeId, 'aspect'),
	getPointcutsAndAdvice(Structure, PointcutList, Advice),	myprint('PoincutList/Advice : ', PointcutList, Advice, 'ATOP'),
	applyPointcuts(PointcutList, Advice, 'before'),
	weaveBeforeAdviceInProcess(BeforeNodeIds).
		
weaveBeforeAdviceInProcess([]).


% Around Advice
% -------------
weaveAroundAdviceInProcess([AroundNodeId | AroundNodeIds]) :-
	generate( Structure, AroundNodeId, 'aspect'),
	getPointcutsAndAdvice(Structure, PointcutList, Advice),	myprint('PoincutList/Advice : ', PointcutList, Advice, 'ATOP'),
	applyPointcuts(PointcutList, Advice, 'around'),
	weaveAroundAdviceInProcess(AroundNodeIds).

weaveAroundAdviceInProcess([]).


% In Advice
% -------------
weaveInAdviceInProcess([InNodeId | InNodeIds]) :-
	generate( Structure, InNodeId, 'aspect'),
	getPointcutsAndAdvice(Structure, PointcutList, Advice),	myprint('PoincutList/Advice : ', PointcutList, Advice, 'ATOP'),
	applyPointcuts(PointcutList, Advice, 'in'),
	weaveInAdviceInProcess(InNodeIds).
weaveInAdviceInProcess([]).

%------------------------


% Separate config from body and return PointcutList as config and Advice as body
getPointcutsAndAdvice( element( Tag, _Atts, Children), [Pointcut], Result) :-
	myprint('Get Pointcuts and Advice... ', 'ATOP'),
	findall(Value, ( member(Key = Value, _Atts), Key='pointcut'), [Pointcut | Atts]),
	findall(Value, ( member(Key = Value, _Atts), Key='joinpoint'), [Joinpoint | Atts]),
	splitPointcut(Pointcut, [], PointcutList), myprint('PointcutList',PointcutList,'ATOP'),
	applyAdditionalPointcuts(PointcutList, Children, Result), myprint('Final Result', Children, Result, 'ATOP'),
%	=(Children,Result),
	myprint('Poincut/PointcutList : ', Pointcut, PointcutList, 'ATOP').


splitPointcut(Pointcut, Init, PointcutList) :-
	split(Pointcut, CurrentTempPointcut, '),', RemainingPointcuts),	concat(CurrentTempPointcut,')',CurrentPointcut),!,
	myprint('CurrentTempPointcut : ', CurrentTempPointcut, 'ATOP'),
	myprint('CurrentPointcut : ', CurrentPointcut, 'ATOP'),
	append(Init, [CurrentPointcut], NewPointcutList),
	splitPointcut( RemainingPointcuts, NewPointcutList, PointcutList).

splitPointcut( FinalPointcut, CurrentList, PointcutList) :-
	append(CurrentList, [FinalPointcut], PointcutList).


applyAdditionalPointcuts(List, Advice, FinalResult) :- applyAdditionalPointcuts(List, Advice, Result, FinalResult).

applyAdditionalPointcuts([],FinalResult,_,FinalResult). 


% Iterate over pointcuts and find additional pointucts.
% Additional pointcut = pointcut that need adapted advice (e.x. to modify the runtime behaviour of the BPEL process)
% T5 =.. T4 pulls brackets from Advice -> solution: add  brackets  in variableValue
applyAdditionalPointcuts([Pointcut | PointcutList], Advice, Result, FinalResult) :-
	myprint('Apply additional Pointcut on: ', Pointcut, PointcutList, 'ATOP'),
	(atom_to_term(Pointcut, T1, B1), T1 =.. L1, append(L1, Advice, L2),flatten(L2,F2), append(F2, Result, L3), flatten(L3,F3), T3 =.. F3 ),
	( checkExistensAdditionalPointcut(Pointcut, T3) ->
	 T3;=(Advice, Result) ),
	applyAdditionalPointcuts(PointcutList, Result, NewResult, FinalResult).

% Check if there is a predicate in the pointcut Library that modifies the advice
checkExistensAdditionalPointcut(Pointcut, AdditionalPointcut) :-
	(clause(AdditionalPointcut, Head)-> true ;(!, fail)).

% Apply PointcutList that refer to predefined pointcuts or directly to library pointcuts
% AFTER POINTCUT
applyPointcut(Advice, Jp, Type, Vars) :-
	myprint('Linking After Advice with Process...: ', Advice, Jp, 'ATOP'),
	(Type == 'after'),!,
	replaceParameters(Advice, Vars, Result),
	myprint('ReplaceParameters: ', Vars, Result,'ATOP'),
	forall(member(R,Result),(parseNextStructure( [R], NextId, 'process'),after(Jp, NextId, 'process'))).
% BEFORE POINTCUT
applyPointcut(Advice, Jp, Type, Vars) :-
	myprint('Linking Before Advice with Process...: ', Advice, Jp, 'ATOP'),
	(Type == 'before'),!,
	replaceParameters(Advice, Vars, Result),
	myprint('ReplaceParameters: ', Vars, Result,'ATOP'),
	forall(member(R,Result),(parseNextStructure( [R], NextId, 'process'),before(Jp, NextId, 'process'))).
% AROUND POINTCUT
applyPointcut(Advice, Jp, Type, Vars) :-
	myprint('Linking Around Advice with Process...: ', Advice, Jp, 'ATOP'),
	(Type == 'around'),!,
	replaceParameters(Advice, Vars, Result),
	myprint('ReplaceParameters: ', Vars, Result,'ATOP'),
        forall(member(R,Result),(parseNextStructure( [R], NextId, 'process'),around(Jp, NextId, 'process'))).
% IN POINTCUT
applyPointcut(Advice, Jp, Type, Vars) :-
	myprint('Linking In Advice with Process...: ', Advice, Jp, 'ATOP'),
	(Type == 'in'),!,
	replaceParameters(Advice, Vars, Result),
	myprint('ReplaceParameters: ', Vars, Result, 'ATOP'),
	replaceAttributes(Result, Jp, NewAdvices),
	myprint('ReplaceAttributes: ', NewAdvices, 'ATOP'),
	forall(member(R,NewAdvices),(parseNextStructure( [R], NextId, 'process'),in(Jp, NextId, 'process'))).
applyPointcut(_, _, _, _).



applyPointcuts([], _, _).

applyPointcuts([Pointcut | PointcutList], Advice, Type) :-
	myprint('Applying Pointcuts... ','ATOP'),
	myprint('Pointcut/PointcutList: ', Pointcut, PointcutList,'ATOP'),
%	correctQuotes(Pointcut, PointcutResult),
%	myprint('CorrectedPointcut: ', PointcutResult, 'ATOP'),
	atom_to_term(Pointcut, T1, [Jp=JpValue | Vars]),
	forall(T1, applyPointcut(Advice,JpValue, Type, Vars)),
	applyPointcuts(PointcutList, Advice, Type).


% Include Using statements
weaveUsingStatements :- 
	includeNameSpaces(['xmlns:pad'='http://schemas.xmlsoap.org/ws/2003/03/business-process/']),
	findall(UsingNodeId, (node(UsingNodeId, NSUsing,'aspect'), split(NSUsing, _, ':','using') ),
		UsingNodeIds), myprint('UsingNodeIds: ', UsingNodeIds,'ATOP'),
	weaveUsingStatement(UsingNodeIds).
	
	
weaveUsingStatement([UsingNodeId|UsingNodeIds]):-
	findall(ChildNodeId, (child(UsingNodeId, ChildNodeId, Order,'aspect')),
		ChildNodeIds), myprint('ChildsNodeIds: ', ChildNodeIds,'ATOP'),

	findall(NameSpaceNodeId, (member(NameSpaceNodeId, ChildNodeIds),node(NameSpaceNodeId, NSNameSpace,'aspect'),
		split(NSNameSpace, _, ':','namespace')),NameSpaceNodeIds), myprint('NameSpaceNodeIds: ', NameSpaceNodeIds,'ATOP'),
	weaveNamespaces(NameSpaceNodeIds),
	findall(PartnerLinkNodeId, (member(PartnerLinkNodeId, ChildNodeIds),node(PartnerLinkNodeId, NSPartnerLink,'aspect'),
		split(NSPartnerLink, _, ':','partnerLink')), PartnerLinkNodeIds), myprint('PartnerLinkNodeIds: ', PartnerLinkNodeIds,'ATOP'),
	weavePartnerLinks(PartnerLinkNodeIds),
	findall(VariableNodeId, (member(VariableNodeId, ChildNodeIds),node(VariableNodeId, NSVariable,'aspect'),
		split(NSVariable, _, ':','variable')), VariableNodeIds), myprint('VariableNodeIds: ', VariableNodeIds,'ATOP'),
	weaveVariables(VariableNodeIds),
	weaveUsingStatement(UsingNodeIds).
 
weaveUsingStatement([]).


%%  NAMESPACES
weaveNamespaces([NameSpaceNodeId|NameSpaceNodeIds]):-
	attribute(NameSpaceNodeId , 'name', NameValue, 'aspect'),
	attribute(NameSpaceNodeId , 'uri', UriValue, 'aspect'),
	includeNameSpaces([NameValue=UriValue]),
	weaveNamespaces(NameSpaceNodeIds).

weaveNamespaces([]).	

%%  PARTNERLINKS
weavePartnerLinks([PartnerLinkNodeId | PartnerLinkNodeIds]):-
	generate(Structure, PartnerLinkNodeId, 'aspect'),
	parseNextStructure([Structure], NextId, 'process'),
	findall(ChildNodeId, (child(0, ChildNodeId, Order,'process')),
		ChildNodeIds), myprint('ChildsNodeIds: ', ChildNodeIds,'ATOP'),
	findall(PartnerLinksNodeId, (member(PartnerLinksNodeId, ChildNodeIds),node(PartnerLinksNodeId, NSPartnerLinks,'process'),
		split(NSPartnerLinks, _, ':','partnerLinks')),[NodeId|PartnerLinksNodeIds]), myprint('PartnerLinksNodeIds: ', NodeId,'ATOP'),
	in(NodeId, NextId, 'process'),
	weavePartnerLinks(PartnerLinkNodeIds).

weavePartnerLinks([]).

%% VARIABLES
weaveVariables([VariableNodeId|VariableNodeIds]):-
	generate(Structure, VariableNodeId, 'aspect'),
	parseNextStructure([Structure], NextId, 'process'),
	findall(ChildNodeId, (child(0, ChildNodeId, Order,'process')),
		ChildNodeIds), myprint('ChildsNodeIds: ', ChildNodeIds,'ATOP'),
	findall(VariablesNodeId, (member(VariablesNodeId, ChildNodeIds),node(VariablesNodeId, NSVariables,'process'),
		split(NSVariables, _, ':','variables')),[NodeId|VariablesNodeIds]), myprint('VariablesNodeIds: ', NodeId,'ATOP'),
	in(NodeId, NextId, 'process'),
	weaveVariables(VariableNodeIds).

weaveVariables([]).




% Include new Namespaces ( xmlns:pred = predValue => compare )
includeNameSpaces([Attr | Atts]) :-
       	myprint('Attributes: ', Attr, 'ATOP'),
	findall( Key = Value, attribute( 0, Key, Value, 'process'), Attributes),	
	addNameSpaceToProcess( Attr, Attributes),
	includeNameSpaces(Atts).

includeNameSpaces([]).

addNameSpaceToProcess(Attr, Attributes):- 
	\+ member(Attr, Attributes), !, assert_properties(0, [Attr],'process').

addNameSpaceToProcess( _, _).

