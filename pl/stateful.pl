testFSA :-
		clear,
		
        defineAspects(['TestAspect'],['demo/aspect/TestAspect.xml']),
        defineProcesses(['test'],['demo/bpel/test.bpel']),
		%deploy('demo/deploymentTest.xml').

        loadAndParseFile('demo/bpel/test.bpel', process),      
        loadAndParseFile('demo/aspect/TestAspect.xml', aspect),
		
		findall(PointcutNodeId, ( node(PointcutNodeId, NSPointcut,'aspect'), (split(NSPointcut, _, ':','pointcut') | NSPointcut = ('pointcut')) ), PointcutNodeIds ),
		assertPointcuts(PointcutNodeIds),
	
		assertFSA.

prepare :-
	%generateFSA(machine1, concat([loop(a), loop(b), choice([c, d])])),
	%generateFSA(machine2, loop(concat([a,b]))),
	%generateFSA(machine3, choice([loop(a), loop(b), c])),
	
	%generateFSA(machine5, concat([a, loop(a)])),
	%generateFSA(machine6, concat([choice([loop(a), loop(b)]), choice([c,d,e])])),
	%generateFSA(machine7, concat([loop(a), loop(b), loop(c)])),

	%% NON-DETERMINISTISCHE FSA !!!!!!
	generateFSA(machine4, choice([concat([a,b]), concat([a,c])])),
	generateFSA(machine8, choice([concat([loop(a), b]), concat([a,b])])),
	generateFSA(machine9, choice([concat([a, loop(a)]), concat([a,b,c]), concat([a,b,d])])),
	
	listing(transitionDFA).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%sequence(Expr) :- pointcuts(N, Expr),
%				  functor(N, MachineName, _),
%				  generateFSA(MachineName, Expr).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% generate FSA met eigen regexp taal ->
%%
%%  vb: generateFSA(name, concat([a,b]))
%%		generateFSA(name, choice([loop(a), concat([b,c])]))
%%		generateFSA(name, loop(a))
%% 		generateFSA(name, loop(choice([a,b])))
%%	
%%  --> simplifyFSA(name).
%%  --> convertNFAToDFA(name)
%%		   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
generateFSA(MachineName, Expr) :-
	assert(startNFA(MachineName, 0)),
	assert(endNFA(MachineName, 1)),
	assert(lastState(MachineName, 1)),	% volgnummer voor de states
	
	makeSubNFA(MachineName, Expr, 0, 1),
	simplifyNFA(MachineName),
	convertNFAToDFA(MachineName),
	
	retract(lastState(MachineName, _)),
	retractall(transitionNFA(MachineName,_,_,_)),
	retractall(startNFA(MachineName,_)),
	retractall(endNFA(MachineName,_)).

makeSubNFA(MachineName, Expr, StartState, EndState) :- 
    pointcuts(Expr, _),
	%atomic(Expr),
	assert(transitionNFA(MachineName, StartState, Expr, EndState)).
		
makeSubNFA(MachineName, concat(Expr), StartState, EndState) :- 
	nextState(MachineName, NewState1),
	nextState(MachineName, NewState2),
				
	assert(transitionNFA(MachineName, StartState, '', NewState1)),
	assert(transitionNFA(MachineName, NewState2, '', EndState)),
	
	processConcat(MachineName, Expr, NewState1, NewState2).
	
makeSubNFA(MachineName, choice(Expr), StartState, EndState) :- 
	processChoice(MachineName, Expr, StartState, EndState).
	
makeSubNFA(MachineName, loop(Expr), StartState, EndState) :- 
	nextState(MachineName, NewState1),
	nextState(MachineName, NewState2),

	assert(transitionNFA(MachineName, StartState, '', NewState1)),
	assert(transitionNFA(MachineName, NewState2, '', EndState)),
	assert(transitionNFA(MachineName, NewState2, '', NewState1)),
	assert(transitionNFA(MachineName, StartState, '', EndState)),
	
	makeSubNFA(MachineName, Expr, NewState1, NewState2).

% minimum 2 elementen in concat	
processConcat(MachineName, [First,Next|Rest], StartState, EndState) :-
	nextState(MachineName, NewState1),
	makeSubNFA(MachineName, First, StartState, NewState1),
	nextState(MachineName, NewState2),
	assert(transitionNFA(MachineName, NewState1, '', NewState2)),
	
	append([Next], Rest, RestExpr),
	processConcat(MachineName, RestExpr, NewState2, EndState).	

% laatste element
processConcat(MachineName, [Last], StartState, EndState) :-
	makeSubNFA(MachineName, Last, StartState, EndState).

processChoice(MachineName, [First|Rest], StartState, EndState) :-
	nextState(MachineName, NewState1),
	nextState(MachineName, NewState2),

	assert(transitionNFA(MachineName, StartState, '', NewState1)),
	makeSubNFA(MachineName, First, NewState1, NewState2),
	assert(transitionNFA(MachineName, NewState2, '', EndState)),
	
	processChoice(MachineName, Rest, StartState, EndState).
	
processChoice(_, [], _,_).

%%% UTIL %%%
nextState(MachineName, NextState) :-
	lastState(MachineName, L),
	next(L, NextState),
	retract(lastState(MachineName, L)),
	assert(lastState(MachineName, NextState)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% simplify NFA : remove e-transitions from non-deterministic automata 
%%%% 
%%%% how to remove e-transitions:
%%%%  http://www.augustana.ca/~jmohr/courses/2006.fall/csc250/slides/PDFChap07.pdf
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
simplifyNFA(MachineName) :-
	removeETransitions(MachineName),
	removeUnreachableStates(MachineName),
	removeDoubleFacts(transitionNFA(MachineName,_,_,_)),
	removeDoubleFacts(endNFA(MachineName,_)).

% herhaaldelijk alle e-transities overlopen (assert/retract wordt pas gecommit bij een backtrack/call), 
% en de empty-transities verwijderen + de nodige final-states toevoegen
removeETransitions(MachineName) :-
	\+ transitionNFA(MachineName, _, '', _); %stoppen als er geen e-transities meer zijn
	
	forall( transitionNFA(MachineName, P, '', Q),
			(removeAndModify(MachineName, P, Q),
			 checkAndSetFinal(MachineName, Q, P))),
			 
	removeETransitions(MachineName).

% (P)-e->(Q) verwijderen
% voor alle (Q)-s->(R) transities, een (P)-s->(R) transities maken
removeAndModify(MachineName, P, Q) :-
	retract(transitionNFA(MachineName, P, '', Q)),
	forall( transitionNFA(MachineName, Q, Symbol, R),
			assert(transitionNFA(MachineName, P, Symbol, R))).

% if Q is a final-state, then set P as a final state, 
% or if Q is not a final-state, then continue.
checkAndSetFinal(MachineName, Q, P) :-
	\+endNFA(MachineName, Q);
	
	endNFA(MachineName, Q),
	assert(endNFA(MachineName, P)).

removeUnreachableStates(MachineName) :-
	forall( (transitionNFA(MachineName, From, S, To),
	         (\+ startNFA(MachineName, From),
	          \+ transitionNFA(MachineName, _, _, From))),
			retract(transitionNFA(MachineName, From, S, To))),
			
	forall( (endNFA(MachineName, EndState), 
	         (\+ transitionNFA(MachineName, EndState, _, _),
              \+ transitionNFA(MachineName, _, _, EndState))),
			retract(endNFA(MachineName, EndState))).	 
											
% verwijder de dubbele facts uit de KB
% template = structuur van de fact.
removeDoubleFacts(Template) :-
	findall(Template, Template, R),
	retractall(Template),
	list_to_set(R, R2),
	addFacts(R2).
	
addFacts([]).
addFacts([Fact|Rest]) :-
	assert(Fact),
	addFacts(Rest).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% NFA -> DFA : make NFA deterministic %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
collectAlphabetNFA(MachineName, Alphabet) :-
	findall(S, transitionNFA(MachineName, _, S, _), A),
	list_to_set(A, Alphabet).

convertNFAToDFA(MachineName) :-
	collectAlphabetNFA(MachineName, Alphabet),
	startNFA(MachineName, StartState),
	assert(startDFA(MachineName, [StartState])),
	makeDFA(MachineName, [StartState], Alphabet).
	
makeDFA(_, [], _).
makeDFA(MachineName, ListStates, Symbols) :-
	loopSymbols(MachineName, ListStates, Symbols, Symbols).

loopSymbols(_, _, [], _).
loopSymbols(_, [], _, _).
loopSymbols(MachineName, ListStates, [S|RestSymbols], Symbols) :-
	reachableMulti(MachineName, ListStates, S, ReachableStates),
	( (createTransition(MachineName, ListStates, S, ReachableStates),
	   makeDFA(MachineName, ReachableStates, Symbols),
	   loopSymbols(MachineName, ListStates, RestSymbols, Symbols));
	   
	  (loopSymbols(MachineName, ListStates, RestSymbols, Symbols))).

% Result = 'reachable' toepassen voor alle states in de NFA
reachableMulti(_, [], _, []).
reachableMulti(MachineName, [From|Rest], Symbol, Result) :-
	reachable(MachineName, From, Symbol, R),
	reachableMulti(MachineName, Rest, Symbol, R2),
	append(R, R2, R3),
	flatten(R3, R4),
	list_to_set(R4, Result).
	
% Result = alle states die bereikbaar zijn vanaf een bepaalde state (From) door hetzelfde
% symbool (Symbol) te consumeren	
reachable(MachineName, From, Symbol, Result) :-
	findall(To, transitionNFA(MachineName, From, Symbol, To), Result).
	
% transitie naar een lege-statelist (error-state) wordt niet gemaakt	
createTransition(_, _, _, []).

% indien er nog geen transitie is, maak er een.
createTransition(MachineName, FromStates, Symbol, ToStates) :-
	\+clause(transitionDFA(MachineName, FromStates, Symbol, ToStates),_), 
	assert(transitionDFA(MachineName, FromStates, Symbol, ToStates)),
	createEndState(MachineName, ToStates).
	
createEndState(MachineName, States) :-
	checkEndState(MachineName, States),
	\+clause(endDFA(MachineName, States),_), %%% --> bij 'simplify' ook testen, zodat er geen dubbele facts meer zijn
	assert(endDFA(MachineName, States)).
	
createEndState(MachineName, States) :-
	\+checkEndState(MachineName, States).
	
checkEndState(MachineName, [S|Rest]) :-
	endNFA(MachineName, S);
	checkEndState(MachineName, Rest).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
collectAlphabetDFA(MachineName, Alphabet) :-
	findall(S, transitionDFA(MachineName, _, S, _), A),
	list_to_set(A, Alphabet).
	
% voor alle protocols, genereer DFA + state-variable
assertFSA :-
	forall( pointcuts(Name, sequence(Expression)),
			(functor(Name, MachineName, _),
			 generateFSA(MachineName, Expression),
			 
			 %voeg een FSA-variable toe aan de usings    
			 node(UsingNodeId, NSUsing, 'aspect'), split(NSUsing, _, ':', 'using'),
			 maxId(MaxId, aspect), next(MaxId, Id),
			 findall(Order, child(UsingNodeId, _, Order, aspect), Orders), 
		 % IF Order=[] THEN NewOrder = 1, ELSE NewOrder = LastOrder + 1
			 (Orders = [] *-> NewOrder = 1 ; maximum(Orders, LastOrder), next(LastOrder, NewOrder)),
			 
			 parse([element(variable, [name=MachineName, type='xsd:string'], [])], UsingNodeId, Id, NextMaxId, NewOrder, aspect)
			  )).   
		    	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		
%%%% applyProtocol = INSERT protocol-advice op de goede plaatsen (= wanneer protocol voldaan is) %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%genereer trace-(after-)advice voor een specifiek pointcut in het protocol
%<AFTER joinpoint="Var" pointcut="transitie(Var)">
% <SWITCH>
%   <CASE ...  er is een case voor iedere transitie met dezelfde pointcut als label 
%   <CASE ... transitie naar end-state: insert aspect-advice
% </SWITCH>
%</AFTER>
%
generateTraceAdvice(MachineName, Pointcut, [element(after, Attributes, [element(switch, [], Cases)])], AspectAdvice) :-
	
	findall(element(case, 
					[condition=C], 
					[element(sequence, [], 
						[element(assign, [], 
							[element(copy, [],
								[element(from, [expression=E], []),
							 	 element(to, [variable=MachineName], [])])])])]),
						 %element(empty, [], [])])]),
							 	 
			(transitionDFA(MachineName, From, Pointcut, To), 
			 \+endDFA(MachineName, To),
			 term_to_atom(From, F),
			 atom_concat('bpws:getVariableData(\'', MachineName, R1),
			 atom_concat(R1, '\') = \'', R2),
			 atom_concat(R2, F, R3),
			 atom_concat(R3, '\'', C),
			 term_to_atom(To, T),
			 atom_concat('\'', T, R4),
			 atom_concat(R4, '\'', E)),
			 			 
			CasesNotEndState),
			
	findall(element(case, 
					[condition=C], 
					[element(sequence, [], 
						[element(assign, [], 
							[element(copy, [],
								[element(from, [expression=E], []),
							 	 element(to, [variable=MachineName], [])])]),
						 A])]),
							 	 
			((transitionDFA(MachineName, From, Pointcut, To), endDFA(MachineName, To)),
			 term_to_atom(From, F),
			 atom_concat('bpws:getVariableData(\'', MachineName, R1),
			 atom_concat(R1, '\') = \'', R2),
			 atom_concat(R2, F, R3),
			 atom_concat(R3, '\'', C),
			 term_to_atom(To, T),
			 atom_concat('\'', T, R4),
			 atom_concat(R4, '\'', E),
			 [A|_] = AspectAdvice),
			 			 
			CasesEndState),
			
	append(CasesNotEndState, CasesEndState, Cases),
	makeAttributes(Pointcut, Attributes).		    
	
	
makeAttributes(Pointcut, Attributes) :-
	term_to_atom(Pointcut, PCut),
	atom_to_term(PCut, _, [Jp=X|Rest]),
	Attributes=[joinpoint=Jp, pointcut=PCut].

generateInitAdvice(MachineName, [element(assign, [], 
									[element(copy, [], 
										[element(from, [expression='\'[0]\''], []),
										 element(to, [variable=MachineName], [])])])]).
	
%%%%%%%
%% weave alle Trace-advices in het process op de goede plaatsen %%		
%%%%%%%
weaveTraceAdvices(MachineName, AspectAdvice) :-
	collectAlphabetDFA(MachineName, Pointcuts),

	forall( member(Pointcut, Pointcuts),
		   (generateTraceAdvice(MachineName, Pointcut, Advice, AspectAdvice),
		    parseNextStructure(Advice, NodeId, aspect),
		    weaveAfterAdviceInProcess([NodeId]))).


% joinpoints die leiden naar een end-state			  
getEndJoinpoints(MachineName, Jp, FromState, EndState) :- 
	%endDFA(MachineName, EndState),
	(transitionDFA(MachineName, FromState, T, EndState), FromState\=EndState, endDFA(MachineName, EndState)),
	term_to_atom(T, At),
	atom_to_term(At, T1, [X=Jp|Rest]),
	T1.	

% joinpoints die vertrekken vanaf  een start-state
getStartJoinpoints(MachineName, Jp) :-
	startDFA(MachineName, StartState),
	(transitionDFA(MachineName, StartState, T, SomeState), SomeState\=StartState),
	term_to_atom(T, At),
	atom_to_term(At, T1, [X=Jp|Rest]),
	T1.
	
% gewone pointcuts en protocol-pointcuts uit elkaar halen.
separateProtocolsFromPointcuts(PointcutList, Protocols, Pointcuts) :-
	findall(Pr, ( member(M, PointcutList), (atom_to_term(M, T, _), pointcuts(T, sequence(_)), term_to_atom(T, Pr))), Protocols),
	findall(Pc, ( member(M, PointcutList), (atom_to_term(M, T, _), pointcuts(T, X), X\=sequence(_), term_to_atom(T, Pc))), Pointcuts).

%%%%%%%
%% weave (extended) protocol-advice op de goede plaatsen %%
%% Hier onderscheidt maken tussen BEFORE en AFTER ('Type'-argument=after/before).  Momenteel is AFTER geimplementeerd
%%%%%%%
applyProtocols([], _, _).
applyProtocols([Protocol | ProtocolList], Advice, Type) :-
	atom_to_term(Protocol, P, _),
	functor(P, MachineName, _),
	
	% init-advice (FSAvariable = [0]) weaven before de eerste pointcut(s) van het protocol
	forall(getStartJoinpoints(MachineName, Jp),
		  (generateInitAdvice(MachineName, InitAdvice),
		   applyPointcut(InitAdvice, Jp, before, []))),
	
	% advices weaven die de huidige state in het protocol bijhouden/bijwerken
	% aspect-advice wordt geweven wanneer er naar een end-state gegaan wordt
	weaveTraceAdvices(MachineName, Advice),
	
	
	
	%%% --->>  in weaveTraceAdvice implementeren
	% aspect-advice weaven bij de pointcuts die naar een end-state leiden
	%forall( getEndJoinpoints(MachineName, Jp, FromState, EndState), 
	        
	%	  (extendAdvice(MachineName, Advice, FromState, ExtendedAdvice),
	%	   applyPointcut(ExtendedAdvice, Jp, Type, []))),
	
	applyProtocols(ProtocolList, Advice, Type).	
	
% een switch rond het advice zetten die controleert of de end-state bereikt is
%extendAdvice(MachineName, Advice, FromState, [element(switch, [], [element(case, [condition=C], Advice)])]) :-
%	%transitionDFA(MachineName, FromState, _, State),
%	%term_to_atom(State, S),
%	term_to_atom(FromState, S),
%	atom_concat('bpws:getVariableData(\'', MachineName, R1),
%	atom_concat(R1, '\') = \'', R2),
%	atom_concat(R2, S, R3),
%	atom_concat(R3, '\'', C).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%sequence([Vars], Expr) :- 
	
	
	
	
enumeratePointcuts([], []).
	
enumeratePointcuts(Expression, [R1, R2, R3]) :-
	findall(M,
			(member(M, Expression), pointcuts(M, _)),
			R1),	
			
	forall(member(choice(Expr), Expression), 
	       enumeratePointcuts(Expr, R2)),
	       
	forall(member(loop(Expr), Expression),
	       enumeratePointcuts(Expr, R3)).
	       
	
		
	


	
	