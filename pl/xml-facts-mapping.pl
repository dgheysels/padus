%% Emacs, this is -*-prolog-*-

:- consult('xml-facts-parse').
:- consult('xml-facts-generate').

%% Use Prolog library to parse xml-files
loadFileInStructureOld(File, Structure) :-
        load_structure(File, Structure, [dialect(xml), space(remove)]).

loadAndParse(File) :-
    loadFileInStructureOld(File, Content),
    parse(Content).

loadAndParseNext(File, NextId) :-
    maxId(Id, 0),
    next(Id, NextId),
    loadFileInStructureOld(File, Content),
    parse(Content, NextId), !.

printStructure(Structure) :-
    write_ln(''),
    current_stream(1, _, Stream),
    xml_write(Stream, Structure, [header(false)]),
    write_ln('').

writeStructure(Structure, File) :-
    open(File,write,Stream),
    current_stream(File, _, Stream),
    write_ln(''),
    xml_write(Stream, Structure, [header(false)]),
    write_ln(''),
    close(Stream).

generateAndPrint :-
    generateAndPrint('process').	
generateAndPrint(Type) :- 
    generate(Structure, Type),
    printStructure(Structure),
    write_ln('').

generateAndWrite(File) :-
    generateAndWrite(File,'process').	
generateAndWrite(File, Type) :- 
    generate(Structure, Type),
    writeStructure(Structure, File),
    write_ln('').

%% Highest Id created so far
maxId(Id, Type) :-
    findall(Id, node(Id, _, Type), Ids),
    maximum(Ids, Id).


maxProcessId(Id, Type) :-
    findall(Id, isAncestor(0, Id, Type) , Ids),
    maximum(Ids, Id).


isAncestor(Ancestor, Child, Type) :-
    child(Ancestor, Child, _, Type),
    \+(Child == Ancestor).

isAncestor(Ancestor, Child, Type) :- 	
    child(Parent, Child, _, Type),
    (Parent \= Child), 
    isAncestor(Ancestor, Parent, Type).
