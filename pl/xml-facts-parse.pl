%% Emacs, this is -*-prolog-*-

:- dynamic node/3.
:- dynamic child/4.
:- dynamic attribute/4.


clear_base :-
	clear_base(_).

clear_base(Type) :-
    retractall(node(_,_,Type)),
    retractall(child(_,_,_,Type)),
    retractall(attribute(_,_,_,Type)),
    retractall(value(_,Type)).

next(Current, Next) :- Next is Current+1.

%% Parse XML structure into flat facts and decouple structure from properties
parse(Structure) :- parse(Structure, 0).
parse(Structure,Root) :- parse(Structure, Root, 0).
parse(Structure, Root, Type) :- parse(Structure, Root, Root, Root, 0, Type).

%% Parse/5 generates a nested list of nodes, short explanation of
%% the variables:
%%   Parent is the ID of the parent node
%%   Id is the ID of the current node
%%   MaxId is the highest ID generated so far (we keep this separately,
%%     so that a node's further sibling nodes can continue with IDs that
%%     follow that of its child nodes)
%%   Order keeps the order of sibling nodes

parse([element(Tag, Attributes, Children) | Siblings],
      Parent, Id, MaxId, Order, Type) :- 
    assert_structure(Tag, Id, Parent, Order, Type),
    assert_properties(Id, Attributes, Type),
    next(Id, NextId),
    parse(Children, Id, NextId, NextMax, 0, Type),
    next(Order, NextOrder),
    parse(Siblings, Parent, NextMax, MaxId, NextOrder, Type).

parse([Value],Parent, Id, NextId, Order, Type) :-
    ([Value]\= [element(Tag, Attributes, Children) | Siblings]),!,
    next(Id, NextId),
    write_ln('value encountered!'),write_ln(Value),
    assert(value(Id,Type)),
    assert_structure(Value, Id, Parent, Order, Type).
  %  assert_properties(Id, 'a=a', Type).

parse( [], _, Max, Max, _, _).
parse([], _, _, _, _, _).

%% SWI-Prolog asserts at the end of the database, keeping implicit order.
%% Keep explicit order, because not sure if this is implementation specific.
assert_structure(Tag, Id, Parent, Order, Type) :- myprint('assert_structure: ',Tag,Id,Parent,'PARSE'),
    assert(node(Id, Tag, Type)),
    assert(child(Parent, Id, Order, Type)). % Use asserta for testing child order


assert_properties(_, [], _).
assert_properties(Id, [Key=Value | Attributes], Type) :- myprint('assert_properties: ',Value,'PARSE'),
    assert(attribute(Id, Key, Value, Type)),
    assert_properties(Id, Attributes, Type).
    

