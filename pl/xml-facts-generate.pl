%% Emacs, this is -*-prolog-*-

%% Generate XML tree from flat facts, starting at root node
generate([Structure]) :-
    generate(Structure, 0).

generate([Structure], Type) :-
    generate(Structure, 0, Type).

%% Generate XML tree, starting at a certain node.
%% First check if node contains attributes, else its a value-node => no further action necessary
generate(Element, Id, Type) :-
	clause(value(A,B),Head) ->
       (value(Id, Type) -> node(Id, Element, Type);generateFull(Element, Id, Type));generateFull(Element, Id, Type). 

%% Expand a node and include in advice
generateWithoutBeforeAfterAround(element(Tag, Attributes, Children), Root, Type) :-
    node(Root, Tag, Type),
    findall(Key=Value, attribute(Root, Key, Value, Type), Attributes),
    findall([Child , Type], (child(Root, Child, _, Type), Root\=Child), ChildIds), myprint(' ','GENERATE'), myprint('ChildIds: ', ChildIds,'GENERATE'),
    predsort(childNodesSorter, ChildIds, TypedSortedIds), myprint('TypedSortedIds: ', TypedSortedIds,'GENERATE'),
    removeType(TypedSortedIds, [], SortedIds),  myprint('SortedIds: ', SortedIds,'GENERATE'),
    includeAfterBeforeAdvice( SortedIds, AfterBeforeIds, Type), myprint('AfterBeforeIds: ', AfterBeforeIds,'GENERATE'),
        myprint('-----------------','GENERATE'),
    includeInAdvice(Root, AfterBeforeIds, AdvisedIds, Type),
%   includeInAdvice(Root, SortedIds, AdvisedIds),
    findall(Child, (member(Id, AdvisedIds), generate(Child, Id, Type)), Children).
	
%% Generate with around advice
generateWithoutBeforeAfter(Element, Root, Type) :-
    advised(Root, around, Around, Type), !,
    generateWithoutBeforeAfterAround(Element, Around, Type).

generateWithoutBeforeAfter(Element, Root, Type) :-
    generateWithoutBeforeAfterAround(Element, Root, Type).


%% Check if there is anything to generate. 
generateFull(Element, Root, Type) :- 
    \+ node(Root, _, Type),
    myprint('Oeps .... empty tree found for Type/Root: ', Type, Root,'GENERATE'),
    Element = [].
   

%% Generate a proceed node: replace with target base node
% Only allow proceed in process space otherwise generating an aspect removes proceeds from advices!
generateFull(Element, Root, 'process') :-
    node(Root, proceed, Type), !,
    proceeds(Root, Advised, Type),
    generateWithoutBeforeAfterAround(Element, Advised, Type).

%% Expand a call to an advice
%generateFull(element(sequence, [], Children), Root, Type) :-
%    node(Root, advice, Type), !,
%    adviceCalls(Root, Called, Type),
%    generateWithoutBeforeAfterAround(element(advice, _, Children), Called, Type).

%% Generate with before/after advice AND add a sequence tag!
generateFull(element(sequence, [], Children), Root, Type) :-
     is_advised(Root, [before, after], Type), !,
     findall(Advice, advised(Root, before, Advice, Type), Befores),
     findall(Advice, advised(Root, after, Advice, Type), Afters),
     append(Befores, [Root|Afters], Sequence),
     findall(Child,
	    (member(Id, Sequence), generateWithoutBeforeAfter(Child, Id, Type)),
	    Children).

%% Standard behavior to expand a node
generateFull(Element, Id, Type) :-
    generateWithoutBeforeAfter(Element, Id, Type).


%% Sort nodes based on child order
childNodesSorter(Delta, [E1 , Type],[E2,Type]) :-
    child(Parent, E1, P1, Type),
    child(Parent, E2, P2, Type),
    compare(Delta, P1, P2).


removeType([[X,_]|Z],Y, NewList):- 
	append(Y, [X], List), 
	removeType(Z, List, NewList).

removeType([], NewList, NewList).



%% Append In advice in child list
includeInAdvice(Id, ChildList, AdvisedChildList, Type) :-
    findall(In, advised(Id, in, In, Type), Ins),
    append(ChildList, Ins, AdvisedChildList). 

%% Include After/Before advice in child list
includeAfterBeforeAdvice( [Child | ChildList], NewAfterBeforeIds, Type):-
	myprint(' **************** ','GENERATE'),
	includeAfterBeforeAdvice( [Child | ChildList], [], NewAfterBeforeIds, Type).

includeAfterBeforeAdvice( [], [], _).

includeAfterBeforeAdvice( [Child | ChildList], AfterBeforeIds, NewAfterBeforeIds, Type) :-
	myprint(' Child: ', Child, ChildList,'GENERATE'),
	findall(Advice, advised( Child, before_ns, Advice, Type), Befores), myprint(' Befores: ', Befores,'GENERATE'),
	findall(Advice, advised( Child, after_ns, Advice, Type), Afters), myprint(' Afters: ', Afters,'GENERATE'),
	append( Befores, [Child|Afters], NewChildList), myprint(' List: ',  NewChildList,'GENERATE'),
	append( AfterBeforeIds, NewChildList, NewAfterBeforeIdsList), myprint(' NewList: ', NewAfterBeforeIdsList,'GENERATE'),
	includeAfterBeforeAdvice(ChildList, NewAfterBeforeIdsList, NewAfterBeforeIds, Type). 

includeAfterBeforeAdvice( [],  NewAfterBeforeIds, NewAfterBeforeIds, Type):- myprint(' End of Iteration: ', NewAfterBeforeIds,'GENERATE').


%% Generate Parameterized Advice
%%---------------------------------
generateWithParameters(Structure, Id,ParameterValues, Type):-
	generate(element(Tag, Attributes, Children), Id, Type),
	replaceParameters(Children, ParameterValues, Structure).

replaceParameters(Children, [Parameter=Value|ParameterValues], Result):-
	findall(Structure, (member(Child,Children),replace([Child], Parameter, Value, [Structure])),Structures),
	replaceParameters(Structures,ParameterValues, Result).

replaceParameters(Result, [], Result). 

% T=Source A=Pattern to replace B=Replacement T1=Result
replace(T,A,B,T1) :-
	concat('$',A,DollarA),
	T =.. [P|TL],myprint('P: ',[P|TL],'GENERATE'),
	( sub_string(P,Start,Length,After,DollarA) ->
	  (string_to_list(P, PList),
	   string_to_list(DollarA, AList),
	   string_to_list(B, BList),
	   substitute_all(PList, AList, BList, X),
	   string_to_list(P1,X));(P1 = P) ),

%	( P == A -> P1 = B ; P1 = P ),
	myprint('P1 A B P: ',P1,A,B,'GENERATE'),
	replaceL(TL,A,B,TL1),
	T1 =.. [P1|TL1].

replaceL([],_,_,[]).
replaceL([T|TL],A,B,[T1|TL1]) :-
	replace(T,A,B,T1),
	replaceL(TL,A,B,TL1).

substitute_all(Source, Tag, Content, X) :-myprint('0: ','GENERATE'),
        append(Tag, After, TagAndAfter),myprint('1: ',Tag,After,TagAndAfter,'GENERATE'),
        append(Before, TagAndAfter, Source),myprint('2: ',Before,TagAndAfter,Source,'GENERATE'),
        append(Before, Content, BeforeAndContent),myprint('3: ',Before,Content,BeforeAndContent,'GENERATE'),
        append(BeforeAndContent, FinalAfter, X),
        !,
        substitute_all(After, Tag, Content, FinalAfter).

substitute_all(Source, _, _, Source).


%% Addapt attributes of joinpoint with in-advice (modify attributes)
% If the selected element does not have an attribute with the specified name, this attribute is added as a new property of the element.
% If the selected element already has an attribute with the specified name, the value of the element is modified.
% If the <attribute> element specifies no value for the property and the selected element has an attribute with the specified name, this attribute is removed. If the selected element has no such attribute, nothing happens.

replaceAttributes(Advices, Jp, NewAdvices) :-
%	write_ln('Not implemented yet!!!').
	myprint('Advices: ', Advices, 'ATOP'),
        findall(Advice, (member(Advice, Advices), \+(modifyJpAttributes([Advice], Jp))),NewAdvices),
	myprint('In-Attribute-Advices: ', NewAdvices, 'ATOP').

modifyJpAttributes([element( NSTag, _Atts, Children)], Jp) :-
	myprint('NSTag: ', NSTag, 'ATOP'),
	split(NSTag, _, ':','attribute'),
	findall(Val, ( member(Key = Val, _Atts), Key='name'), [Name | Atts]),
	findall(Val, ( member(Key = Val, _Atts), Key='value'), [Value | Atts]),
	myprint('Name/Value/Jp: ',Name, Value, Jp, 'ATOP'),
	((Value='',attribute(Jp,Name,RemoveValue,'process')) ->
	   (myprint('case1: Remove!','ATOP'),retract(attribute(Jp,Name,RemoveValue,'process')));
	   (attribute(Jp,Name,ModifyValue,'process') ->
	     (myprint('case2:modify!','ATOP'),retract(attribute(Jp,Name,ModifyValue,'process')),assert(attribute(Jp, Name, Value, 'process')));
	     (myprint('case3:add!','ATOP'),assert(attribute(Jp, Name, Value, 'process'))))).
		

